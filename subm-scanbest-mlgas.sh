#!/bin/bash

#SBATCH --partition=general
#SBATCH --ntasks=1
#SBATCH --time=48:00:00

module use unstable
module load anaconda/2019.07
conda activate talos_py36

cd /psi/home/feichtinger/jupyterhub/ml-gasmon
python swissfell-gas-df-scanbest.py
