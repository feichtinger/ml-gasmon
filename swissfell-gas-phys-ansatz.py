# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python [conda env:datascience_py37]
#     language: python
#     name: conda-env-datascience_py37-py
# ---

# + {"toc": true, "cell_type": "markdown"}
# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#Configuration" data-toc-modified-id="Configuration-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Configuration</a></span></li><li><span><a href="#Support-Routines" data-toc-modified-id="Support-Routines-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Support Routines</a></span><ul class="toc-item"><li><span><a href="#Visualization" data-toc-modified-id="Visualization-2.1"><span class="toc-item-num">2.1&nbsp;&nbsp;</span>Visualization</a></span></li></ul></li><li><span><a href="#Dataset-creation" data-toc-modified-id="Dataset-creation-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Dataset creation</a></span><ul class="toc-item"><li><span><a href="#Dataset-reading-and-preprocessing-definition" data-toc-modified-id="Dataset-reading-and-preprocessing-definition-3.1"><span class="toc-item-num">3.1&nbsp;&nbsp;</span>Dataset reading and preprocessing definition</a></span></li><li><span><a href="#Make-dataset" data-toc-modified-id="Make-dataset-3.2"><span class="toc-item-num">3.2&nbsp;&nbsp;</span>Make dataset</a></span></li></ul></li><li><span><a href="#Examining-data" data-toc-modified-id="Examining-data-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>Examining data</a></span><ul class="toc-item"><li><span><a href="#Observations-from-data-set-inspection" data-toc-modified-id="Observations-from-data-set-inspection-4.1"><span class="toc-item-num">4.1&nbsp;&nbsp;</span>Observations from data set inspection</a></span></li></ul></li><li><span><a href="#physics-based-model" data-toc-modified-id="physics-based-model-5"><span class="toc-item-num">5&nbsp;&nbsp;</span>physics based model</a></span><ul class="toc-item"><li><span><a href="#Equations-for-average-number-of-photons-per-pulse" data-toc-modified-id="Equations-for-average-number-of-photons-per-pulse-5.1"><span class="toc-item-num">5.1&nbsp;&nbsp;</span>Equations for average number of photons per pulse</a></span></li><li><span><a href="#Equations-for-Electromultiplier-response" data-toc-modified-id="Equations-for-Electromultiplier-response-5.2"><span class="toc-item-num">5.2&nbsp;&nbsp;</span>Equations for Electromultiplier response</a></span></li><li><span><a href="#Additional-relationship-to-measure-constant-B-of-the-EM-gain-equation" data-toc-modified-id="Additional-relationship-to-measure-constant-B-of-the-EM-gain-equation-5.3"><span class="toc-item-num">5.3&nbsp;&nbsp;</span>Additional relationship to measure constant B of the EM gain equation</a></span></li><li><span><a href="#Data-set-preparation" data-toc-modified-id="Data-set-preparation-5.4"><span class="toc-item-num">5.4&nbsp;&nbsp;</span>Data set preparation</a></span></li><li><span><a href="#Xe-data-on-cross-section,-charge-vs-photon-energy" data-toc-modified-id="Xe-data-on-cross-section,-charge-vs-photon-energy-5.5"><span class="toc-item-num">5.5&nbsp;&nbsp;</span>Xe data on cross section, charge vs photon energy</a></span></li><li><span><a href="#Nphot---avg-number-of-photons-per-pulse" data-toc-modified-id="Nphot---avg-number-of-photons-per-pulse-5.6"><span class="toc-item-num">5.6&nbsp;&nbsp;</span>Nphot - avg number of photons per pulse</a></span></li><li><span><a href="#Nphot-vs-CALCS-for-some-narrow-supply-Voltage-ranges" data-toc-modified-id="Nphot-vs-CALCS-for-some-narrow-supply-Voltage-ranges-5.7"><span class="toc-item-num">5.7&nbsp;&nbsp;</span>Nphot vs CALCS for some narrow supply Voltage ranges</a></span></li></ul></li><li><span><a href="#Model1:-fitting-the-Electromultiplier-gain-equation" data-toc-modified-id="Model1:-fitting-the-Electromultiplier-gain-equation-6"><span class="toc-item-num">6&nbsp;&nbsp;</span>Model1: fitting the Electromultiplier gain equation</a></span><ul class="toc-item"><li><span><a href="#Optimization-of-a-suitable-subrange" data-toc-modified-id="Optimization-of-a-suitable-subrange-6.1"><span class="toc-item-num">6.1&nbsp;&nbsp;</span>Optimization of a suitable subrange</a></span></li><li><span><a href="#Optimization-on-the-full-data-set" data-toc-modified-id="Optimization-on-the-full-data-set-6.2"><span class="toc-item-num">6.2&nbsp;&nbsp;</span>Optimization on the full data set</a></span></li><li><span><a href="#using-cutoff-for-CALCS-<-4000-uJ" data-toc-modified-id="using-cutoff-for-CALCS-<-4000-uJ-6.3"><span class="toc-item-num">6.3&nbsp;&nbsp;</span>using cutoff for CALCS &lt; 4000 uJ</a></span></li></ul></li><li><span><a href="#Model-2:-semi-empirical" data-toc-modified-id="Model-2:-semi-empirical-7"><span class="toc-item-num">7&nbsp;&nbsp;</span>Model 2: semi-empirical</a></span><ul class="toc-item"><li><span><a href="#fitting-an-empirical-correction-to-the-gain-equation" data-toc-modified-id="fitting-an-empirical-correction-to-the-gain-equation-7.1"><span class="toc-item-num">7.1&nbsp;&nbsp;</span>fitting an empirical correction to the gain equation</a></span></li></ul></li><li><span><a href="#DESK-WORKSPACE" data-toc-modified-id="DESK-WORKSPACE-8"><span class="toc-item-num">8&nbsp;&nbsp;</span>DESK WORKSPACE</a></span></li><li><span><a href="#deduce-information-from-CALCS-distribution-as-function-of-voltage" data-toc-modified-id="deduce-information-from-CALCS-distribution-as-function-of-voltage-9"><span class="toc-item-num">9&nbsp;&nbsp;</span>deduce information from CALCS distribution as function of voltage</a></span></li><li><span><a href="#Looking-at-relationship-of-CALCS-and-CALCT" data-toc-modified-id="Looking-at-relationship-of-CALCS-and-CALCT-10"><span class="toc-item-num">10&nbsp;&nbsp;</span>Looking at relationship of CALCS and CALCT</a></span></li><li><span><a href="#Regressions-using-CALCT" data-toc-modified-id="Regressions-using-CALCT-11"><span class="toc-item-num">11&nbsp;&nbsp;</span>Regressions using CALCT</a></span><ul class="toc-item"><li><span><a href="#Model3:-simple-EM-gain-curve-for-CALCT" data-toc-modified-id="Model3:-simple-EM-gain-curve-for-CALCT-11.1"><span class="toc-item-num">11.1&nbsp;&nbsp;</span>Model3: simple EM gain curve for CALCT</a></span></li><li><span><a href="#Model-4:-semi-empirical" data-toc-modified-id="Model-4:-semi-empirical-11.2"><span class="toc-item-num">11.2&nbsp;&nbsp;</span>Model 4: semi-empirical</a></span></li></ul></li></ul></div>

# +
import os
import re
import json
import array
import random
import pickle
import time
from math import sqrt

import numpy as np
import pandas as pd
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
import scipy.stats
#matplotlib inline
# -

import itertools
import glob

# # Configuration

#topdir = "/psi/home/adelmann/data/ml-gasmon/"
topdir = "/psi/home/feichtinger/jupyterhub/ml-gasmon"
directory = os.path.join(topdir, "cleaned/")
xlsxFn    = os.path.join('XeX77.xlsx')


# # Support Routines

def print_model_err(model, xt, yt):
    y_pred=model.predict(xt)
    print('Mean Absolute Error:', metrics.mean_absolute_error(yt, y_pred))
    print('Mean Squared Error:', metrics.mean_squared_error(yt, y_pred))
    print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(yt, y_pred)))


# ## Visualization

# +
def calc_gaussian_kde(x, y):
    xy = np.vstack((x, y))
    f = scipy.stats.gaussian_kde(xy)
    return f(xy)

def densitycolor(x, y):
    # calculate point density
    density = calc_gaussian_kde(x, y)
    # sort points according to density values
    indices = density.argsort()
    return x[indices], y[indices], density[indices]

def coloredscatter(x, y, alpha=1.0, cmap="Blues", ax=None):
    if ax is None:
        fig, ax = plt.subplots()

    x, y, z = densitycolor(x, y)
    paths = ax.scatter(x, y, c=z, s=50, edgecolor='', alpha=alpha, cmap=cmap)
    plt.colorbar(paths, ax=ax)


# +
#found online for axis formatting
import matplotlib.ticker as mticker

class MathTextSciFormatter(mticker.Formatter):
    def __init__(self, fmt="%1.2e"):
        self.fmt = fmt
    def __call__(self, x, pos=None):
        s = self.fmt % x
        decimal_point = '.'
        positive_sign = '+'
        tup = s.split('e')
        significand = tup[0].rstrip(decimal_point)
        sign = tup[1][0].replace(positive_sign, '')
        exponent = tup[1][1:].lstrip('0')
        if exponent:
            exponent = '10^{%s%s}' % (sign, exponent)
        if significand and exponent:
            s =  r'%s{\times}%s' % (significand, exponent)
        else:
            s =  r'%s%s' % (significand, exponent)
        return "${}$".format(s)


# -

# # Dataset creation

# ## Dataset reading and preprocessing definition

def makeDataSetInterpolated(directory, excelFn, doInterpolate=True, dropBadPulses=True, verbose=False,
                           CALCTthreshold=-50, CALCSthreshold=-50):
    first = True
    data = []
    for filename in sorted(os.listdir(directory)):
        if filename.endswith(".csv"):
            fntmp = re.sub(r'.*dp', '', filename)
            expNumber = re.sub(r'-nomeans.csv', '', fntmp)
            file_excel = pd.read_excel(excelFn)
            multVoltag = file_excel.iloc[int(expNumber)]['XeMultVoltag'] 
            try:
                dp  = pd.read_csv(directory+filename, sep=";") 
            except:
                print ("Can not read " + directory + filename)
                continue
                
            dp = dp[['SARFE10-PBIG050-EVR0:CALCT.value', 
                    'SARFE10-PBIG050-EVR0:CALCS.value',
                    'SARFE10-PSSS059:SPECTRUM_CENTER.value',
                    'SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.value']]
            dp.columns = ['CALCT','CALCS','SPECTRUM_CENTER','PHOTON-ENERGY-PER-PULSE']

            if doInterpolate:
                dp['PHOTON-ENERGY-PER-PULSE'].interpolate(method='linear', 
                                                              inplace=True, 
                                                              limit_direction='forward', 
                                                              axis=0)
            
            dp = dp.dropna();

            # condition for bad pulse
            if dropBadPulses:
                validT = dp['CALCT'] < CALCTthreshold
                validS = dp['CALCS'] < CALCSthreshold
                dp = dp[validT & validS]
            
            dp['XeMultVoltag'] = multVoltag
            dp['rawDataFile']  = filename
            
            if first:
                data = dp
                first = False
            else:
                data = data.append(dp,ignore_index=True)

            if verbose:
                print("Datapoint", expNumber, "gave", len(dp), "values")
    data.reset_index(inplace=True)
    data.dropna()
    return data

# The initial DNN regression runs led to the identification of measurement artifacts where the measured average pulse energy was stored as zero, heavily influencing the interpolated pulse energies in their neighborhood.
#
# In the following data cleaning, the zeros are replaced by NaN and all interpolation is done based on the two surrounding values. The correctness of this procedure is unclear, since if there really was no beam in the vicinity of this measurement one maybe should rather not use the points of those regions at all

def makeDataSetInterpolated2(directory, excelFn, doInterpolate=True, dropBadPulses=True, verbose=False,
                           CALCTthreshold=-50, CALCSthreshold=-50):
    data = pd.DataFrame()
    for filename in sorted(glob.glob(os.path.join(directory,'dp*-nomeans.csv'))):
        mat = re.match('^dp(\d+).*', os.path.basename(filename))
        if mat is None:
            print(f"WARNING: Could not parse number part of filename {os.path.basename(filename)}")
            continue
        expNumber = int(mat.group(1))
        file_excel = pd.read_excel(excelFn)
        multVoltag = file_excel.iloc[expNumber]['XeMultVoltag'] 
        try:
            dp  = pd.read_csv(filename, sep=";") 
        except:
            print (f"WARNING: Can not read {os.path.basename(filename)}. skipping...")
            continue

        dp = dp[['SARFE10-PBIG050-EVR0:CALCT.value', 
                'SARFE10-PBIG050-EVR0:CALCS.value',
                'SARFE10-PSSS059:SPECTRUM_CENTER.value',
                'SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.value']]
        dp.columns = ['CALCT','CALCS','SPECTRUM_CENTER','PHOTON-ENERGY-PER-PULSE']

        # Replace zeroes by NaN
        fzeroes = dp.loc[dp['PHOTON-ENERGY-PER-PULSE'] == 0.0]
        if fzeroes.shape[0] > 0:
            print(f"zeroes found in PEPP-avg of {os.path.basename(filename)} at {fzeroes.index.values}")
            dp.loc[dp['PHOTON-ENERGY-PER-PULSE'] == 0.0, 'PHOTON-ENERGY-PER-PULSE'] = np.NaN

        if doInterpolate:
            dp['PHOTON-ENERGY-PER-PULSE'].interpolate(method='linear', 
                                                          inplace=True, 
                                                          limit_direction='forward', 
                                                          axis=0)

        dp = dp.dropna();

        # condition for bad pulse
        if dropBadPulses:
            dp = dp.query('CALCT < @CALCTthreshold & CALCS < @CALCSthreshold')

        dp['XeMultVoltag'] = multVoltag
        dp['rawDataFile']  = os.path.basename(filename)
        
        data = pd.concat([data,dp])

        if verbose:
            print("Datapoint", expNumber, "gave", len(dp), "values")

    data.dropna()
    data.reset_index(inplace=True)
    return data

# ## Make dataset 
#
# (Can not read /psi/home/adelmann/SwissFEL-Gas-1/cleaned/dp41-nomeans.csv is ok)

data      = makeDataSetInterpolated2(directory,xlsxFn,CALCTthreshold=-50,CALCSthreshold=-50,verbose=False)

data.info()

data.describe()

data.tail()

#nr files actually used
data['rawDataFile'].nunique()

# Some data files contain almost no measurement points

fig, ax = plt.subplots(figsize=(16,5))
data['rawDataFile'].value_counts().sort_index().plot(kind='bar', ax=ax)
tmp = ax.set_xticklabels(ax.get_xticklabels(),rotation=45,horizontalalignment='right')


var_indep = ['SPECTRUM_CENTER','XeMultVoltag','CALCT','CALCS']
var_dep = ['PHOTON-ENERGY-PER-PULSE']

# -
# # Examining data

# Information on the variables based on mail by Pavle Juranic:
#    - **CALCS and CALCT** are actually in uJ, or pulse energy.  The actual
#      waveform we have is Vs, since it's an integral of a waveform, but
#      the end value that we wish to get is uJ, which it is directly
#      related to.
#    - The **photon-energy-per-pulse-avg** is also in uJ, but after it's
#      been averaged over about 30 seconds and divided by the repetition
#      rate of the machine.
#    - **Spectrum-center** is the FEL beam energy in eV.
#    - (**XeMultVoltag** is the preset electromultiplier voltage)
#

# Typical values?
#
# XePhotEnergyL = 6000.     # Ev
# XePhotEnergyH = 12500.
#
# XeMultVoltagL = 600.      # V
# XeMultVoltagH = 1600.
#
# XePulseEnergL = 10        # uJ
# XePulseEnergH = 500

sns.pairplot(data[['CALCT', 'CALCS', 'SPECTRUM_CENTER','XeMultVoltag','PHOTON-ENERGY-PER-PULSE']])

# Just plotting the data from a few files shows that within the files the data have very little variation as compared to between files.

# +
fchoice = "01,02,03,05,08,09,10,35,36,64,65,66,76"
colname = ["PHOTON-ENERGY-PER-PULSE","XeMultVoltag","SPECTRUM_CENTER","CALCS","CALCT"]
flist = [f'rawDataFile == "dp{fnum}-nomeans.csv"' for fnum in fchoice.split(",")]

tmpdata = data.query(" | ".join(flist)) \
                        .drop(labels="index", axis=1) \
                        .reset_index(drop=True).reset_index()
for col in colname:
    sns.lmplot(x="index", y=col, hue="rawDataFile",
              data=tmpdata,
              fit_reg=False, height=4, aspect=3)

# -

# Rendering the variables for all files and samples

#sns.lmplot(data=data.drop(labels='index',axis=1).reset_index(drop=True).reset_index(),
#           x='index',y='PHOTON-ENERGY-PER-PULSE', hue='rawDataFile',
#          fit_reg=False)
fig = plt.figure(figsize=(16,36))
for num,col in enumerate(colname):
    ax = fig.add_subplot(len(colname),1,num+1)
    for fname in sorted(data.rawDataFile.unique()):
        data.query('rawDataFile == @fname')[col] \
            .plot(marker='o', linestyle='',ax=ax)
    ax.set_xlabel('sample index')
    ax.set_ylabel(col)
fig.tight_layout()

# Let's identify files with high variation in the target PEPP variable. Can we see some patterns in the other variables for these files (e.g. a more strongly varying CALCS/CALCT, since the other variables are basically constant).

# +
# get files list with high variation in PEPP
flist = data.groupby('rawDataFile') \
    .agg({'PHOTON-ENERGY-PER-PULSE': ['min','max']})['PHOTON-ENERGY-PER-PULSE'] \
    .assign(diff = lambda x: x['max'] - x['min']) \
    .query('diff > 30').index.to_list()

flist = [f'rawDataFile == "{fnum}"' for fnum in flist]

tmpdata = data.query(" | ".join(flist)) \
                        .drop(labels="index", axis=1) \
                        .reset_index(drop=True).reset_index()

colname = ["PHOTON-ENERGY-PER-PULSE","XeMultVoltag","SPECTRUM_CENTER","CALCS","CALCT"]
for col in colname:
    sns.lmplot(x="index", y=col, hue="rawDataFile",
              data=tmpdata,
               palette=sns.color_palette("Set1", len(flist)),
              fit_reg=False, height=4, aspect=3)

# -

# In the PEPP vs CALCT plot the points of PEPP ~ 200 stick out and seem to disturb the
# general pattern exhibited by the rest of the data. The variation of the CALCT values also is greatest, there. Let's have a closer look.

#data.plot(x='PHOTON-ENERGY-PER-PULSE', y='CALCT', marker='.', linestyle='')
sns.lmplot(x="PHOTON-ENERGY-PER-PULSE", y='CALCT', hue="rawDataFile",
              data=data,
               palette=sns.color_palette("Set1", len(flist)),
              fit_reg=False, height=4, aspect=3, legend=False)


sns.lmplot(x="PEPP", y='CALCT', hue="rawDataFile",
              data=data.rename(columns = {'PHOTON-ENERGY-PER-PULSE': 'PEPP'}) \
                        .query('180 < PEPP < 250'),
               palette=sns.color_palette("Set1", len(flist)),
              fit_reg=False, height=4, aspect=3, legend=True)


# So, the points are all part of file dp05

# ## Observations from data set inspection
#    * data have very little variation within each file as compared to between files (as explained by the experimental setup).
#    * **XeMultVoltag** is constant over a file, **Spectrum_Center** has small variation within one file.
#    * The interpolated target value **PEPP** (photon energy per pulse) varies just slightly within files, with a few files showing some moderate variation.
#    * **Spectrum_Center** values grow monotonically from the first to the last file.
#       * according to the experimental setup. 
#    * **CALCS and CALCT** are strongly correlated (q.v. correlation plot further below). They show the biggest relative variations of all variables within one file.


# # physics based model

# ## Equations for average number of photons per pulse
# The number of photons per time based on the longer interval current measurement done at the upstream/downstream electrodes is given by this formula
#
# \begin{aligned} N_{ph} = \frac{0.862iT}{\sigma z_i p \gamma} \end{aligned}
#
# It is derived from formula (1) in cite:tiedtke-2008-gas-detec
#
# \begin{aligned} \frac{I_{\text {ion }}}{\Phi}=\frac{q(\hbar \omega) \sigma(\hbar \omega) z_{\text {ion }} \eta_{\text {ion }} p}{\hbar \omega k T} \end{aligned}
# using $q(\hbar \omega) = \gamma(\hbar \omega) e$
#
#    
# so we arrive at this formula for the avg. number of photons per second (I am using $z = z_{\text {ion }} \eta_{\text {ion }}$ )
#
# \begin{aligned} N_{ph} = \frac{\Phi}{\hbar \omega} = \frac{k}{e} \dot{} \frac{I_{\text {ion}} T}{\gamma \sigma z p} \end{aligned}
#
# The *avg. photon energy per pulse (PEP)*, using $\nu$ as the FEL pulse frequency
#
#
# \begin{aligned} {\text {PEP}}_{\text {avg}} =  {\hbar \omega}\frac{N_{ph}}{\nu} = \frac{k}{e} \dot{} \frac{{\hbar \omega} I_{\text {ion}} T}{\gamma \sigma z p \nu} \end{aligned}
#           

# ## Equations for Electromultiplier response
# The *gain* function of an electromultiplier that is operated within the linear range can be modeled by
#
# \begin{aligned} gain = const_1 \cdot V^{const_2 \cdot n} \end{aligned}
# where V is the Electromultiplier's supply voltage (XeMultVolag in the dataset) and n is the number of dynode stages. The two constants are instrument specific (q.v. https://www.hamamatsu.com/resources/pdf/etd/EMT_TPMH1354E.pdf).
#
# \begin{aligned} {\text CALC} = N_{\text ion} \cdot const_1 \cdot V^{const_2 \cdot n} \end{aligned}
#
# We do not know $N_{\text ion}$, the number of absolute incident ions that is hitting the electromultiplier, but we know that it will be proporional to the number of incident photons $N_{\text {ph}}$. So, I am choosing the following model function for fitting the data
#
# \begin{aligned} {\text CALC} = N_{\text ph} \cdot A \cdot V^{B} \end{aligned}
#
# with A and B being model parameters to be fit to the data.
#

# ## Additional relationship to measure constant B of the EM gain equation
#
# The variation of the electromultiplier response grows with the supply voltage.
#
# For constant supply voltage, the gain is constant (within the linear electromultiplier range). So, if we assume that the number of photons per pulse is gaussian distributed, the CALCS values should also be gaussian distributed. We can use the standard deviation of each experiment series and use this relation
#
# \begin{aligned} \frac{\Delta \text {CALC}_0}{\Delta \text {CALC}_1} = \frac{\Delta N_0 \cdot V_0^B}{\Delta N_1 \cdot V_1^B} \end{aligned}
#
# The distribution of the number of photo ions between the experiments is certainly independent of the supply voltage, and hopefully also mostly independent of the other variables, so $\Delta N_0 \approx \Delta N_1$. This then yields upon rearrangement
#
# \begin{aligned}
# ln(\frac{\Delta \text {CALC}_0}{\Delta \text {CALC}_1}) = B \cdot ln(\frac{\Delta V_0}{\Delta V_1})
# \end{aligned}

# ## Data set preparation
#
# The high variation in the CALCS and CALCT reflect the real variation of produced Ions between pulses, and so the true PEP values should proportionally show the same variations. Therefore I consider it inappropriate to use interpolated PEP values for training the model. I rather consider the samples within each file as one experiment (as it was set up to have the same avg PEP over the course of the experiment) and use the mean values of CALCS/CALCT.
#

# data.drop('index', axis=1).groupby('rawDataFile').agg(['mean','std'])
data_mean = data.drop('index', axis=1).groupby('rawDataFile').mean()

# data.drop('index', axis=1).groupby('rawDataFile').agg(['mean','std'])
data_mean = data.drop('index', axis=1).groupby('rawDataFile').mean()
data_mean = pd.concat([data_mean.rename(columns={'CALCS':'realcalcs'}),
                       data.drop('index', axis=1) \
                          .groupby('rawDataFile')['CALCS'] \
                          .count()], axis=1) \
          .rename(columns={'CALCS':'count', 'realcalcs':'CALCS'})

# data.drop('index', axis=1).groupby('rawDataFile').agg(['mean','std'])
data_mean = data.drop('index', axis=1).groupby('rawDataFile').mean()
data_mean = pd.concat([data_mean.rename(columns={'CALCS':'realcalcs'}),
                       data.drop('index', axis=1) \
                          .groupby('rawDataFile')['CALCS'] \
                          .agg(count='count', CALCSstd='std')], axis=1) \
          .rename(columns={'realcalcs':'CALCS'})

data_mean.reset_index().head(3)

# ## Xe data on cross section, charge vs photon energy
# To play with all the data, I am adding columns for the Xenon cross section and the average charge per ion. I look at the tabulated values that the cross section and the avg charge take over the range of measured photon energies in the data. 

xedf = pd.read_csv('./backgroundref/Xe.DAT', delim_whitespace=True, comment='#',
                  skiprows=[1]) \
         .rename(columns={"Xe": "cross-section", "Xe.gamma": "charge"})

xedf.head()

# +
# prepare dataframe containing photon energies from data set
tmpdf = pd.DataFrame()
tmpdf['Energy'] = data_mean.SPECTRUM_CENTER
tmpdf['cross-section'] = np.NaN
#tmpdf['rawDataFile'] = 'placeholder'
tmpdf['charge'] = np.NaN
tmpdf.set_index('Energy', inplace=True)

# Join with Xe data set for interpolation
tmpdf = pd.concat([tmpdf, xedf.reset_index(drop=True).set_index('Energy')], sort=True)
tmpdf.head()
# -

tmpdf = tmpdf.sort_index() \
     .interpolate(method='index')

# test whether interpolation has worked
tmpdf.loc[(tmpdf.index > 6100) & (tmpdf.index < 6105)]

fig,ax = plt.subplots(figsize=(12,6))
tmpdf['cross-section'].plot(marker='.', ax=ax)
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_ylabel('cross section / Mb')

# +
physdf = pd.concat([data_mean.reset_index().set_index('SPECTRUM_CENTER'), tmpdf], axis=1, join='inner') \
        .reset_index() \
        .rename(columns = {'index': 'SPECTRUM_CENTER'})

physdf.head()
# -

fig,ax = plt.subplots(figsize=(12,6))
physdf.plot(x='SPECTRUM_CENTER', y='cross-section',
            linestyle='', marker='o', ax=ax, legend=False)
#ax.set_xscale('log')
#ax.set_yscale('log')
ax.set_xlabel('SPECTRUM_CENTER / eV')
ax.set_ylabel('cross section / Mb')
ax.set_title("Xe cross section across data's photon energy range")

physdf.XeMultVoltag.plot(linestyle='', marker='o')

# ## Nphot - avg number of photons per pulse
#
# The Electromultiplier signals like CALCS should be proportional to the number of photoions hitting the detector (multiplier was operated within the linear voltage range). We do not know the number of photons hitting the EM, but we know that it must be proportional to the number of photons $N_{ph}$ in the beam, which we obtain from the ion current detector.
#
#  \begin{aligned} N_{ph} = \frac{\Phi}{\hbar \omega} = \frac{k}{e} \dot{} \frac{I_{\text {ion}} T}{\gamma \sigma z p} \end{aligned}
#  
# In the present data set we already get the avg. photon energy per pulse, so we can get the average photons per pulse directly by
#  
# \begin{aligned}
# N_{\text{ph, pulse}} = \frac{{\text {PEP}}_{\text {avg}}} {\hbar \omega}
# \end{aligned}
#
# Since PEP is given in micro Joule and beam energy (SPECTRUM_CENTER) in eV, we need to convert
#
# 1 uJ/eV = 6.241509e+12

fconv_eV_to_uJ = 6.241509e12

physdf['Nphot_avg'] = fconv_eV_to_uJ * physdf['PHOTON-ENERGY-PER-PULSE'] / physdf['SPECTRUM_CENTER']


physdf.describe()

fig,ax = plt.subplots(figsize=(12,6))
#physdf.plot(kind='scatter', x='CALCT', y='Nsigma_avg', ax=ax[0], alpha=0.5)
physdf.plot(kind='scatter', x='CALCT', y='Nphot_avg', ax=ax, alpha=0.5)

physdf.XeMultVoltag.sort_values().reset_index(drop=True).plot(linestyle='', marker='o')

# Let's look at a sorted plot of EM Voltage, and how the photonE correlates
fig,axes=plt.subplots(1,2, figsize=(12,6))
physdf.sort_values(by='XeMultVoltag').reset_index(drop=True) \
      .plot(y='XeMultVoltag',linestyle='', marker='o', ax=axes[0])
physdf.sort_values(by='XeMultVoltag').reset_index(drop=True) \
      .plot(y='SPECTRUM_CENTER',linestyle='', marker='o', ax=axes[1])

# ## Nphot vs CALCS for some narrow supply Voltage ranges
# In order to have a first impression whether our data agrees with the basic electromultiplier response function, I try to find samples where the electromultiplier voltage (XeMultVoltag) is mostly constant, and I look at the relation of the the avg Number of photons per pulse vs. CALCS.

# +
tmpdf = physdf.query('1190 < XeMultVoltag < 1230')
markerseq = itertools.cycle(('+', 'v', 'x', 's', 'D', 'X'))
agrid = sns.lmplot(x='Nphot_avg', y='CALCS', hue="rawDataFile",
           markers=[next(markerseq) for _ in tmpdf.XeMultVoltag.unique()],
              data=tmpdf,
              fit_reg=False, height=4, aspect=3)

for row in range(0, tmpdf.shape[0]):
    #agrid.axes[0][0].text(tmpdf.Nphot_avg.iloc[0]+0.002, tmpdf.CALCS.iloc[0], "test")
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.0005, tmpdf.CALCS.iloc[row],
                  f'V:{np.round(tmpdf.XeMultVoltag.iloc[row],1)}')    
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.0005, tmpdf.CALCS.iloc[row]-440,
                  f'E:{np.round(tmpdf.SPECTRUM_CENTER.iloc[row],1)}')    


# +
tmpdf = physdf.query('850 < XeMultVoltag < 950')
markerseq = itertools.cycle(('+', 'v', 'x', 's', 'D', 'X'))
agrid = sns.lmplot(x='Nphot_avg', y='CALCS', hue="rawDataFile",
           markers=[next(markerseq) for _ in tmpdf.XeMultVoltag.unique()],
              data=tmpdf,
              fit_reg=False, height=4, aspect=3)

for row in range(0, tmpdf.shape[0]):
    #agrid.axes[0][0].text(tmpdf.Nphot_avg.iloc[0]+0.002, tmpdf.CALCS.iloc[0], "test")
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.0005, tmpdf.CALCS.iloc[row],
                  f'V:{np.round(tmpdf.XeMultVoltag.iloc[row],1)}')    
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.0005, tmpdf.CALCS.iloc[row]-50,
                  f'E:{np.round(tmpdf.SPECTRUM_CENTER.iloc[row],1)}')
# -


# In the above graph, the dp01 file is a marked outlier. It has almost the same Voltage as dp32 (926). It is taken a lower photon energy (6101).

# +
tmpdf = physdf.query('1400 < XeMultVoltag < 1530')
markerseq = itertools.cycle(('+', 'v', 'x', 's', 'D', 'X'))
agrid = sns.lmplot(x='Nphot_avg', y='CALCS', hue="rawDataFile",
           markers=[next(markerseq) for _ in tmpdf.Nphot_avg],
              data=tmpdf,
              fit_reg=False, height=4, aspect=3)

for row in range(0, tmpdf.shape[0]):
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.002, tmpdf.CALCS.iloc[row],
                  np.round(tmpdf.XeMultVoltag.iloc[row],1))    
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.002, tmpdf.CALCS.iloc[row]-1100,
                  np.round(tmpdf.SPECTRUM_CENTER.iloc[row],1))    

# -

# # Model1: fitting the Electromultiplier gain equation

# \begin{aligned} {\text CALC} = N_{\text ph} \cdot A \cdot V^{B} \end{aligned}
#

# # copy of our data for experimenting
physdf1 = physdf.copy()


# x input is an array of 2 arrays: [nphot, voltage]
def calcs_fn(x, kcoeff, kpow):
    return x[0] * kcoeff * np.power(x[1], kpow) 


#Testing the function with some dummy numbers
calcs_fn([ physdf1.Nphot_avg.iloc[0:2], physdf1.XeMultVoltag.iloc[0:2] ],                                    
         -40, 1.0)

# ## Optimization of a suitable subrange
# First try a optimization on a subrange of electromultiplier supply voltages

tmpdf = physdf1.query('1400 < XeMultVoltag < 1530')
popt, pcov = scipy.optimize.curve_fit(calcs_fn,
                                      [ tmpdf.Nphot_avg, tmpdf.XeMultVoltag ],
                                      tmpdf.CALCS,
                                      [-40.0, 1.0],
                                      maxfev=10000
                                     )

# Fitted values for A and B 
popt

fig, ax = plt.subplots()
ax.plot(tmpdf.Nphot_avg, tmpdf.CALCS, label='data',linestyle='', marker='o')
ax.plot(tmpdf.Nphot_avg, calcs_fn([tmpdf.Nphot_avg, tmpdf.XeMultVoltag], popt[0], popt[1]),
         label='fit', linestyle='', marker='o')
ax.legend()

# ## Optimization on the full data set

popt, pcov = scipy.optimize.curve_fit(calcs_fn,
                                      [ physdf1.Nphot_avg, physdf1.XeMultVoltag ],
                                      physdf1.CALCS,
                                      [-40.0, 1.0],
                                      maxfev=10000
                                     )

# **Results for the fitted constants A and B of the gain equation**

popt

# Let's plot the gain function based on these parameters. Note, that it is not the true gain function of the electromultiplier, because we have it in function of $N_{ph}$ and not of $N_{ion}$.

fig,ax = plt.subplots()
v_ax = np.linspace(physdf1.XeMultVoltag.min(),physdf1.XeMultVoltag.max(), 100)
# we get the gain by setting Nphot=1.0
ax.plot(v_ax, -calcs_fn([np.full(v_ax.shape, 1.0 ), v_ax], popt[0], popt[1]))
ax.set_yscale('log')
ax.set_xlabel('XeMultVoltag')
ax.set_ylabel('Gain')

# the predictions for CALCS given the experiments Nphot [from PEP / h \omega)]
# predictions for PEP based on the measured CALCS
physdf1 = physdf1.assign(CALCS_pred = lambda x: calcs_fn([x.Nphot_avg, x.XeMultVoltag], popt[0], popt[1]),
                       PEP_pred = lambda x: (1/fconv_eV_to_uJ) * x.SPECTRUM_CENTER * x.CALCS / (popt[0] * np.power(x.XeMultVoltag, popt[1])))

fig, ax = plt.subplots()
ax.plot(physdf1.Nphot_avg, physdf1.CALCS, label='data',linestyle='', marker='o')
ax.plot(physdf1.Nphot_avg, physdf1.CALCS_pred,
         label='fit', linestyle='', marker='o')
ax.set_xlabel('Nphot')
ax.set_ylabel('CALCS')
ax.legend()


physdf1.columns

# measured vs. predicted CALCS
fig,ax = plt.subplots(figsize=(8,4))
ax.plot(physdf1.CALCS, physdf1.CALCS_pred,
         linestyle='', marker='o', alpha=0.6)
ax.set_xlabel('CALCS measured')
ax.set_ylabel('CALCS predicted')

# measured vs. predicted
fig,ax = plt.subplots(figsize=(8,4))
ax.plot(physdf1['PHOTON-ENERGY-PER-PULSE'], physdf1.PEP_pred,
         linestyle='', marker='o', alpha=0.6)
ax.set_xlabel('PEP measured')
ax.set_ylabel('PEP predicted')

# Let's see whether there is some pattern in relation to the other variables by adding a color mapping

# +
# measured vs. predicted
tmp_xcol = physdf1['CALCS']
tmp_ycol = physdf1['CALCS_pred']
tmp_color = physdf1.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')
ax.set_xlabel('CALCS measured')
ax.set_ylabel('CALCS predicted')
ax.set_title('Model 1: CALCS predicted vs measured, beam energy dependence')
# -

# **There clearly is a systematic dependence on the beam energy.** The CALCS predictions for higher beam energies are systematically too high in this fit. No such systematic dependency is visible if we look at the influence of the EM Voltage (plot below).
#
# The dependency on the beam energy above suggests that we could introduce a simple correction term as a function of beam energy. But one should think about what this dependence really signifies in terms of the physics.
#

# +
# measured vs. predicted
tmp_xcol = physdf1['CALCS']
tmp_ycol = physdf1['CALCS_pred']
tmp_color = physdf1.XeMultVoltag

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('EM Voltage')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')
ax.set_xlabel('CALCS measured')
ax.set_ylabel('CALCS predicted')
ax.set_title('Model 1: CALCS predicted vs measured, EM Voltage dependence')
# -

# In the plot above no simple dependence on the EM Voltage is visible.
#
# Below, we transform the result into the PEP predictions space.

# +
# measured vs. predicted
tmp_xcol = physdf1['PHOTON-ENERGY-PER-PULSE']
tmp_ycol = physdf1['PEP_pred']
tmp_color = physdf1.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')

ax.set_xlabel('PEP measured')
ax.set_ylabel('PEP predicted')
ax.set_title('Model 1: PEP predicted vs measured, beam energy dependence')

# +
# measured vs. predicted
tmp_xcol = physdf1['CALCS']
tmp_ycol = physdf1['PEP_pred'] - physdf1['PHOTON-ENERGY-PER-PULSE']
tmp_color = physdf1.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')

ax.set_xlabel('CALCS')
ax.set_ylabel('PEP predicted - PEP avg.')
ax.set_title('Model 1: PEP Error, Residual plot')
# -

# ## using cutoff for CALCS < 4000 uJ
# Pavle wrote on 23. March that we probably should discard values of CALC > -4000 

physdf1_cut = physdf1.query('CALCS > -4000').copy()

popt_cut, pcov_cut = scipy.optimize.curve_fit(calcs_fn,
                                      [ physdf1_cut.Nphot_avg, physdf1_cut.XeMultVoltag ],
                                      physdf1_cut.CALCS,
                                      [-5e-16, 6.0],
                                      maxfev=10000
                                     )

# **Results for the fitted constants A and B of the gain equation**

popt_cut

# the predictions for CALCS given the experiments Nphot [from PEP / h \omega)]
# predictions for PEP based on the measured CALCS
physdf1_cut = physdf1_cut.assign(CALCS_pred = lambda x: calcs_fn([x.Nphot_avg, x.XeMultVoltag], popt_cut[0], popt_cut[1]),
                       PEP_pred = lambda x: (1/fconv_eV_to_uJ) * x.SPECTRUM_CENTER * x.CALCS / (popt_cut[0] * np.power(x.XeMultVoltag, popt_cut[1])))

fig, ax = plt.subplots()
ax.plot(physdf1_cut.Nphot_avg, physdf1_cut.CALCS, label='data',linestyle='', marker='o')
ax.plot(physdf1_cut.Nphot_avg, physdf1_cut.CALCS_pred,
         label='fit', linestyle='', marker='o')
ax.set_xlabel('Nphot')
ax.set_ylabel('CALCS')
ax.legend()


# +
# measured vs. predicted
tmp_xcol = physdf1_cut['CALCS']
tmp_ycol = physdf1_cut['CALCS_pred']
tmp_color = physdf1_cut.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')
ax.set_xlabel('CALCS measured')
ax.set_ylabel('CALCS predicted')
ax.set_title('Model 1 (cut): CALCS predicted vs measured, beam energy dependence')

# +
# measured vs. predicted
tmp_xcol = physdf1_cut['PHOTON-ENERGY-PER-PULSE']
tmp_ycol = physdf1_cut['PEP_pred']
tmp_color = physdf1_cut.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')

ax.set_xlabel('PEP measured')
ax.set_ylabel('PEP predicted')
ax.set_title('Model 1 (cut): PEP predicted vs measured, beam energy dependence')
# -

physdf1_cut

# # Model 2: semi-empirical 
# ## fitting an empirical correction to the gain equation
# Based on the beam energy dependency of the predictions from the EM gain curve, it seems that a simple correction as a function of beam energy can produce a better fit. I try with a primitive correction factor linearly dependent on the beam energy.
#
# N.B.: this is just a simple test inspired by math. Maybe we can find a correction term grounded in physics.

# \begin{aligned} {\text CALC} = (1 + C \cdot E_{\text {beam}} ) \cdot N_{\text {ph}} \cdot A \cdot V^{B} \end{aligned}
#
# and by using
#
# \begin{aligned}
# N_{\text{ph, pulse}} = \frac{{\text {PEP}}_{\text {avg}}} {\hbar \omega}
# \end{aligned}
#
# \begin{aligned}
# {\text {PEP}}_{\text {pred}} = \frac{{\text CALC} \cdot E_{\text {beam}}}{(1 + C \cdot E_{\text {beam}}) A V^{B}}
# \end{aligned}
#

#
#
#

# # copy of our data for experimenting
physdf2 = physdf.copy()


# x input is composed of 3 arrays: [ nphot, voltage, Ebeam]
def calcs_fn2(x, kcoeff, kpow, ecoeff):
    return (1 + x[2] * ecoeff) * x[0] * kcoeff * np.power(x[1], kpow) 


#Testing the function with some dummy numbers
calcs_fn2([physdf2.Nphot_avg.iloc[0:2],
           physdf2.XeMultVoltag.iloc[0:2],
          physdf2.SPECTRUM_CENTER.iloc[0:2]],                                    
         -40, 1.0, 1e-4)

popt2, pcov2 = scipy.optimize.curve_fit(calcs_fn2,
                                      [physdf2.Nphot_avg,
                                       physdf2.XeMultVoltag,
                                       physdf2.SPECTRUM_CENTER],
                                      physdf2.CALCS,
                                      [-40.0, 1.0, 1.0e-8],
                                      maxfev=10000
                                     )

# **Results for the fitted constants A, B, and C of the modified gain equation**

popt2

pcov2

# Since C is negative, it signifies that the same number of incident photons (~ photoions) produce a weaker EM signal at higher beam energies.
#
# What could be the physical explanation for this? The number of photons for this fitting we get from the longer-timescale electrode measurements. Is there a difference in the Ion production at the electrodes or near the electromultipliers that could account for the beam energy dependence?

# Visualizing the beam energy dependent gain curves and comparing to the simeple electromultiplier gain fit from above:

fig,ax = plt.subplots(figsize=(12,8))
v_ax = np.linspace(physdf1.XeMultVoltag.min(),physdf1.XeMultVoltag.max(), 100)
# we get the gain by setting Nphot=1.0
ax.plot(v_ax, -calcs_fn([np.full(v_ax.shape, 1.0 ), v_ax], popt[0], popt[1]),
       label='M1: EM gain', linestyle='--')
for enrg in [7000, 9000, 11000]:
    ax.plot(v_ax, -calcs_fn2([np.full(v_ax.shape, 1.0 ), v_ax, np.full(v_ax.shape, enrg )],
            popt2[0], popt2[1], popt2[2]), label=f'M2: E={enrg} eV')
ax.set_yscale('log')
ax.set_xlabel('XeMultVoltag')
ax.set_ylabel('Gain')
ax.legend()


# \begin{aligned} \text{PEP}_{\text{pred}} = \frac{\text {CALC} \cdot E_{\text {beam}}}{(1 + C \cdot E_{\text {beam}} ) \cdot A \cdot V^{B}} \end{aligned}

# function factory for these models
def mk_pep_pred_fn2(A, B, C):
    return lambda calcs, voltage, ebeam: (1/6.241509e12) * ebeam * calcs / (1 + C * ebeam) / (A * np.power(voltage, B))


pep_pred_fn2 = mk_pep_pred_fn2(popt2[0], popt2[1], popt2[2])

# the predictions for CALCS given the experiments Nphot [from PEP / h \omega)]
# predictions for PEP based on the measured CALCS
physdf2 = physdf2.assign(CALCS_pred = lambda x: calcs_fn2([x.Nphot_avg,
                                                           x.XeMultVoltag,
                                                           x.SPECTRUM_CENTER], popt2[0], popt2[1], popt2[2]),
                       PEP_pred = lambda x: pep_pred_fn2(x.CALCS, x.XeMultVoltag, x.SPECTRUM_CENTER))

# +
# measured vs. predicted
tmp_xcol = physdf2['CALCS']
tmp_ycol = physdf2['CALCS_pred']
tmp_color = physdf2.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')
ax.set_xlabel('CALCS measured')
ax.set_ylabel('CALCS predicted')
ax.set_title('Model 2: CALCS predicted vs measured, beam energy dependence')

# +
# measured vs. predicted
tmp_xcol = physdf2['PHOTON-ENERGY-PER-PULSE']
tmp_ycol = physdf2['PEP_pred']
tmp_color = physdf2.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')

ax.set_xlabel('PEP measured')
ax.set_ylabel('PEP predicted')
ax.set_title('Model 2: PEP predicted vs measured, beam energy dependence')
# -

# N.B.: The fit looks good, but we need to find out whether the points at high beam energy are real outliers, or whether it's just an artifact of my new modulated fit function.

# # DESK WORKSPACE

physdf2.loc[physdf2.XeMultVoltag > 1500, ['CALCS','PHOTON-ENERGY-PER-PULSE', 'XeMultVoltag','SPECTRUM_CENTER']] \
       .sort_values('CALCS')

# some tests
pep_pred_fn2(-1019.149371, 1571.98, 9884.956165)

# # deduce information from CALCS distribution as function of voltage
# (this test I had done earlier... it is an interesting complementary way, but I guess without much practical impact, since the uncertainties are large)
#
# As described above, we can try to obtain the constant B from the gain formula by looking at the variation of the CALCS values in function of the supply voltage
#
# \begin{aligned}
# ln(\frac{\Delta \text {CALC}_0}{\Delta \text {CALC}_1}) = B \cdot ln(\frac{\Delta V_0}{\Delta V_1})
# \end{aligned}


# Sort the data by supply voltage values

physdf = physdf.sort_values(by='XeMultVoltag').reset_index(drop=True)

# I do not want to use measurement series with less than 100 samples

physdf = physdf.query('count > 100')

physdf.plot(y='XeMultVoltag',linestyle='', marker='o')

# The following plots show that there must be some additional underlying effect for explaining the variation of CALCS with the supply voltage. Still, I proceed with this analysis.

fig,ax = plt.subplots(figsize=(12,6))
data_vsort = data.sort_values('XeMultVoltag').reset_index(drop=True)
for fname in physdf.rawDataFile:
    data_vsort.query('rawDataFile == @fname')['CALCS'] \
            .plot(marker='o', linestyle='',ax=ax)
ax.set_xlabel('samples in order of increasing supply voltage')


ax = physdf.plot(x='XeMultVoltag', y="CALCSstd", linestyle='', marker='o')

# Check whether sample series distributions are approximately gaussian. Turns out that they are not quite, but still the stdev is maybe not such a bad parameter for the estimation.

samples = [0, 1, 20,21, physdf.shape[0]-2,physdf.shape[0]-1]
fig = plt.figure(figsize=(12, np.ceil(len(samples))*3))
for counter,i in enumerate(samples):
    ax = fig.add_subplot(np.ceil(len(samples)),2,counter+1)
    sns.distplot(data.query('rawDataFile == @physdf.iloc[@i].rawDataFile').CALCS,
                 bins=20, ax=ax, fit=scipy.stats.norm)
    ax.set_title(f'{physdf.iloc[i].rawDataFile}, V={np.round(physdf.iloc[i].XeMultVoltag)}')
    ax.set_xlabel('CALCS')
fig.tight_layout()    


# Create columns for fitting this model
# \begin{aligned}
# ln(\frac{\Delta \text {CALC}_0}{\Delta \text {CALC}_1}) = B \cdot ln(\frac{\Delta V_0}{\Delta V_1})
# \end{aligned}

physdf = physdf.assign(lncalc = lambda x: np.log(x['CALCSstd'] / physdf.iloc[0].CALCSstd),
              lnvolt = lambda x: np.log(x['XeMultVoltag'] / physdf.iloc[0].XeMultVoltag))

physdf.head()

sns.regplot(x='lnvolt',y='lncalc', data=physdf)

# Note: linreg is not really appropriate (intercept must be 0), but for this short test its ok

lreg = scipy.stats.linregress(physdf.lnvolt, physdf.lncalc)

lreg.slope, lreg.intercept

# So, this method gave 6.12 for the parameter B, while the regression of the simple gain equation (earlier section) gave 6.5.

# # Looking at relationship of CALCS and CALCT
# (unfinished)

# Looking at the response from both electromultipliers. What is the expected relation if the gain equation is valid?
#
# \begin{aligned}
# \frac{\text CALCS}{\text CALCT} = \frac{N_{\text ph} \cdot A_S \cdot V^{B_S}}{N_{\text ph} \cdot A_T \cdot V^{B_T}}
# = \frac{A_S}{A_T} \cdot V^{B_S - B_T}
# \end{aligned}
#
# If both electromultipliers are very similar, then $B_S \approx B_T$ and there should be a linear relationship between CALCS and CALCT when operated at the same Voltage. In the data, we see a linear relationship over a wide range of the lower response function. Towards the higher values (abs(CALCS) > 15000) there is a "bend" in the relationship.

fig,ax = plt.subplots(figsize=(12,6))
ax.plot(data.CALCS, data.CALCT, linestyle='', marker='o',alpha=0.05)
ax.set_xlabel('CALCS')
ax.set_ylabel('CALCT')

fig,ax = plt.subplots(2,1,figsize=(12,6))
datatmp = data.query('CALCS > -15000') \
              .assign(calcs_d_t = lambda x: x.CALCS/x.CALCT)
ax[0].plot(datatmp.CALCS, datatmp.CALCT, linestyle='', marker='o',alpha=0.05)
ax[0].set_xlabel('CALCS')
ax[0].set_ylabel('CALCT')
ax[1].plot(datatmp.XeMultVoltag,datatmp.calcs_d_t, linestyle='', marker='o', alpha=0.05)
ax[1].set_ylabel('CALCS/CALCT')
ax[1].set_xlabel('supply Voltage')


# +
#fig,ax = plt.subplots(2,1,figsize=(12,6))
#datatmp = data.query('CALCS > -15000') \
#              .assign(calcs_d_t = lambda x: x.CALCS/x.CALCT)

#ax[0].plot(datatmp.CALCS, datatmp.CALCT, linestyle='', marker='o',alpha=0.05)
#ax[0].set_xlabel('CALCS')
#ax[0].set_ylabel('CALCT')
#ax[1].plot(datatmp.XeMultVoltag,datatmp.calcs_d_t, c=datatmp.SPECTRUM_CENTER, linestyle='', marker='o', alpha=0.05)
#ax[1].set_ylabel('CALCS/CALCT')
#ax[1].set_xlabel('supply Voltage')

# -

fig,ax = plt.subplots(figsize=(12,6))
ax.plot(data.CALCS/data.CALCT, data.XeMultVoltag, linestyle='', marker='o', alpha=0.05)

fig,ax = plt.subplots(figsize=(12,6))
datatmp = data.query('CALCS > -15000')
ax.plot(datatmp.CALCS/datatmp.CALCT, datatmp.SPECTRUM_CENTER, linestyle='', marker='o', alpha=0.05)

# # Regressions using CALCT
# It is not clear whether XeMultVoltag reflects the Voltage of the Upstream or Downstream Electromultiplier (CALCS is the readout from the Upstream EM, CALCT from the downstream one).
# The new data I got seems off scale. So, I am doing the fits again with CALCT and XeMultVoltage in order to find out whether that mismatch is due to using the wrong EM voltage.

# ## Model3: simple EM gain curve for CALCT

# # copy of our data for experimenting
physdf3 = physdf.copy()


# x input is an array of 2 arrays: [nphot, voltage]
def calct_fn(x, kcoeff, kpow):
    return x[0] * kcoeff * np.power(x[1], kpow) 


#Testing the function with some dummy numbers
calct_fn([ physdf1.Nphot_avg.iloc[0:2], physdf1.XeMultVoltag.iloc[0:2] ],                                    
         -40, 1.0)

popt3, pcov3 = scipy.optimize.curve_fit(calct_fn,
                                      [ physdf3.Nphot_avg, physdf3.XeMultVoltag ],
                                      physdf3.CALCT,
                                      [-40.0, 1.0],
                                      maxfev=10000
                                     )

# **Results for the fitted constants A and B of the gain equation**

popt3

# Let's plot the gain function based on these parameters. Note, that it is not the true gain function of the electromultiplier, because we have it in function of $N_{ph}$ and not of $N_{ion}$.

fig,ax = plt.subplots()
v_ax = np.linspace(physdf3.XeMultVoltag.min(),physdf3.XeMultVoltag.max(), 100)
# we get the gain by setting Nphot=1.0
ax.plot(v_ax, -calct_fn([np.full(v_ax.shape, 1.0 ), v_ax], popt3[0], popt3[1]))
ax.set_yscale('log')
ax.set_xlabel('XeMultVoltag')
ax.set_ylabel('Gain')

# the predictions for CALCT given the experiments Nphot [from PEP / h \omega)]
# predictions for PEP based on the measured CALCT
physdf3 = physdf3.assign(CALCT_pred = lambda x: calct_fn([x.Nphot_avg, x.XeMultVoltag], popt3[0], popt3[1]),
                       PEP_pred = lambda x: (1/fconv_eV_to_uJ) * x.SPECTRUM_CENTER * x.CALCT / (popt3[0] * np.power(x.XeMultVoltag, popt3[1])))

fig, ax = plt.subplots()
ax.plot(physdf3.Nphot_avg, physdf3.CALCT, label='data',linestyle='', marker='o')
ax.plot(physdf3.Nphot_avg, physdf3.CALCT_pred,
         label='fit', linestyle='', marker='o')
ax.set_xlabel('Nphot')
ax.set_ylabel('CALCT')
ax.legend()


# Let's see whether there is some pattern in relation to the other variables by adding a color mapping

# +
# measured vs. predicted
tmp_xcol = physdf3['CALCT']
tmp_ycol = physdf3['CALCT_pred']
tmp_color = physdf3.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')
ax.set_xlabel('CALCT measured')
ax.set_ylabel('CALCT predicted')
ax.set_title('Model 3: CALCT predicted vs measured, beam energy dependence')
# -

# **There clearly is a systematic dependence on the beam energy.** The CALCS predictions for higher beam energies are systematically too high in this fit. No such systematic dependency is visible if we look at the influence of the EM Voltage (plot below).
#
# The dependency on the beam energy above suggests that we could introduce a simple correction term as a function of beam energy. But one should think about what this dependence really signifies in terms of the physics.
#

# +
# measured vs. predicted
tmp_xcol = physdf3['CALCT']
tmp_ycol = physdf3['CALCT_pred']
tmp_color = physdf3.XeMultVoltag

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('EM Voltage')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')
ax.set_xlabel('CALCT measured')
ax.set_ylabel('CALCT predicted')
ax.set_title('Model 3: CALCT predicted vs measured, EM Voltage dependence')
# -

# In the plot above no simple dependence on the EM Voltage is visible.
#
# Below, we transform the result into the PEP predictions space.

# +
# measured vs. predicted
tmp_xcol = physdf3['PHOTON-ENERGY-PER-PULSE']
tmp_ycol = physdf3['PEP_pred']
tmp_color = physdf3.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')

ax.set_xlabel('PEP measured')
ax.set_ylabel('PEP predicted')
ax.set_title('Model 3: PEP predicted vs measured, beam energy dependence')

# +
# measured vs. predicted
tmp_xcol = physdf3['CALCT']
tmp_ycol = physdf3['PEP_pred'] - physdf3['PHOTON-ENERGY-PER-PULSE']
tmp_color = physdf3.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')

ax.set_xlabel('CALCT')
ax.set_ylabel('PEP predicted - PEP avg.')
ax.set_title('Model 1: PEP Error, Residual plot')
# -

# ## Model 4: semi-empirical 
# Based on the beam energy dependency of the predictions from the EM gain curve, it seems that a simple correction as a function of beam energy can produce a better fit. I try with a primitive correction factor linearly dependent on the beam energy.
#
# N.B.: this is just a simple test inspired by math. Maybe we can find a correction term grounded in physics.

# \begin{aligned} {\text CALC} = (1 + C \cdot E_{\text {beam}} ) \cdot N_{\text {ph}} \cdot A \cdot V^{B} \end{aligned}
#
# and by using
#
# \begin{aligned}
# N_{\text{ph, pulse}} = \frac{{\text {PEP}}_{\text {avg}}} {\hbar \omega}
# \end{aligned}
#
# \begin{aligned}
# {\text {PEP}}_{\text {pred}} = \frac{{\text CALC} \cdot E_{\text {beam}}}{(1 + C \cdot E_{\text {beam}}) A V^{B}}
# \end{aligned}
#

#
#
#

# # copy of our data for experimenting
physdf4 = physdf.copy()


# x input is composed of 3 arrays: [ nphot, voltage, Ebeam]
def calct_fn4(x, kcoeff, kpow, ecoeff):
    return (1 + x[2] * ecoeff) * x[0] * kcoeff * np.power(x[1], kpow) 


#Testing the function with some dummy numbers
calct_fn4([physdf4.Nphot_avg.iloc[0:2],
           physdf4.XeMultVoltag.iloc[0:2],
          physdf4.SPECTRUM_CENTER.iloc[0:2]],                                    
         -40, 1.0, 1e-4)

popt4, pcov4 = scipy.optimize.curve_fit(calct_fn4,
                                      [physdf4.Nphot_avg,
                                       physdf4.XeMultVoltag,
                                       physdf4.SPECTRUM_CENTER],
                                      physdf4.CALCT,
                                      [-40.0, 1.0, 1.0e-8],
                                      maxfev=10000
                                     )

# **Results for the fitted constants A, B, and C of the modified gain equation**

popt4

pcov4

# Since C is negative, it signifies that the same number of incident photons (~ photoions) produce a weaker EM signal at higher beam energies.
#
# What could be the physical explanation for this? The number of photons for this fitting we get from the longer-timescale electrode measurements. Is there a difference in the Ion production at the electrodes or near the electromultipliers that could account for the beam energy dependence?

# Visualizing the beam energy dependent gain curves and comparing to the simeple electromultiplier gain fit from above:

fig,ax = plt.subplots(figsize=(12,8))
v_ax = np.linspace(physdf1.XeMultVoltag.min(),physdf4.XeMultVoltag.max(), 100)
# we get the gain by setting Nphot=1.0
ax.plot(v_ax, -calct_fn([np.full(v_ax.shape, 1.0 ), v_ax], popt3[0], popt3[1]),
       label='M1: EM gain', linestyle='--')
for enrg in [7000, 9000, 11000]:
    ax.plot(v_ax, -calct_fn4([np.full(v_ax.shape, 1.0 ), v_ax, np.full(v_ax.shape, enrg )],
            popt4[0], popt4[1], popt4[2]), label=f'M2: E={enrg} eV')
ax.set_yscale('log')
ax.set_xlabel('XeMultVoltag')
ax.set_ylabel('Gain')
ax.legend()


# \begin{aligned} \text{PEP}_{\text{pred}} = \frac{\text {CALC} \cdot E_{\text {beam}}}{(1 + C \cdot E_{\text {beam}} ) \cdot A \cdot V^{B}} \end{aligned}

# function factory for these models
def mk_pep_pred_fn4(A, B, C):
    return lambda calct, voltage, ebeam: (1/6.241509e12) * ebeam * calct / (1 + C * ebeam) / (A * np.power(voltage, B))


pep_pred_fn4 = mk_pep_pred_fn4(popt4[0], popt4[1], popt4[2])

# the predictions for CALCT given the experiments Nphot [from PEP / h \omega)]
# predictions for PEP based on the measured CALCT
physdf4 = physdf4.assign(CALCT_pred = lambda x: calct_fn4([x.Nphot_avg,
                                                           x.XeMultVoltag,
                                                           x.SPECTRUM_CENTER], popt4[0], popt4[1], popt4[2]),
                       PEP_pred = lambda x: pep_pred_fn4(x.CALCT, x.XeMultVoltag, x.SPECTRUM_CENTER))

# +
# measured vs. predicted
tmp_xcol = physdf4['CALCT']
tmp_ycol = physdf4['CALCT_pred']
tmp_color = physdf4.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')
ax.set_xlabel('CALCT measured')
ax.set_ylabel('CALCT predicted')
ax.set_title('Model 4: CALCT predicted vs measured, beam energy dependence')

# +
# measured vs. predicted
tmp_xcol = physdf4['PHOTON-ENERGY-PER-PULSE']
tmp_ycol = physdf4['PEP_pred']
tmp_color = physdf4.SPECTRUM_CENTER

fig,ax = plt.subplots(figsize=(12,6))
sc = ax.scatter(tmp_xcol, tmp_ycol,
          c=tmp_color)
colbar = fig.colorbar(sc, ax=ax)
colbar.set_label('beam energy (SPECTRUM_CENTER) / eV')
ax.plot([tmp_xcol.min(),tmp_xcol.max()], [tmp_xcol.min(),tmp_xcol.max()], linestyle='--')

ax.set_xlabel('PEP measured')
ax.set_ylabel('PEP predicted')
ax.set_title('Model 4: PEP predicted vs measured, beam energy dependence')
# -


