# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python [conda env:talos_py36]
#     language: python
#     name: conda-env-talos_py36-py
# ---

# + {"toc": true, "cell_type": "markdown"}
# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#Configuration" data-toc-modified-id="Configuration-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Configuration</a></span></li><li><span><a href="#Support-Routines" data-toc-modified-id="Support-Routines-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Support Routines</a></span><ul class="toc-item"><li><span><a href="#Visualization" data-toc-modified-id="Visualization-2.1"><span class="toc-item-num">2.1&nbsp;&nbsp;</span>Visualization</a></span></li></ul></li><li><span><a href="#Dataset-creation" data-toc-modified-id="Dataset-creation-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Dataset creation</a></span><ul class="toc-item"><li><span><a href="#Dataset-reading-and-preprocessing-definition" data-toc-modified-id="Dataset-reading-and-preprocessing-definition-3.1"><span class="toc-item-num">3.1&nbsp;&nbsp;</span>Dataset reading and preprocessing definition</a></span></li><li><span><a href="#Make-dataset" data-toc-modified-id="Make-dataset-3.2"><span class="toc-item-num">3.2&nbsp;&nbsp;</span>Make dataset</a></span></li><li><span><a href="#Original-Training/Test-Split:-Random-over-all-samples" data-toc-modified-id="Original-Training/Test-Split:-Random-over-all-samples-3.3"><span class="toc-item-num">3.3&nbsp;&nbsp;</span>Original Training/Test Split: Random over all samples</a></span></li><li><span><a href="#Data-scaling-for-DNN-training" data-toc-modified-id="Data-scaling-for-DNN-training-3.4"><span class="toc-item-num">3.4&nbsp;&nbsp;</span>Data scaling for DNN training</a></span></li></ul></li><li><span><a href="#Examining-data" data-toc-modified-id="Examining-data-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>Examining data</a></span><ul class="toc-item"><li><span><a href="#Observations-from-data-set-inspection" data-toc-modified-id="Observations-from-data-set-inspection-4.1"><span class="toc-item-num">4.1&nbsp;&nbsp;</span>Observations from data set inspection</a></span></li></ul></li><li><span><a href="#DNN-Model-definitions" data-toc-modified-id="DNN-Model-definitions-5"><span class="toc-item-num">5&nbsp;&nbsp;</span>DNN Model definitions</a></span><ul class="toc-item"><li><span><a href="#L2reg-and-gaussian-noise" data-toc-modified-id="L2reg-and-gaussian-noise-5.1"><span class="toc-item-num">5.1&nbsp;&nbsp;</span>L2reg and gaussian noise</a></span></li><li><span><a href="#L1-reg-and-gaussian-noise" data-toc-modified-id="L1-reg-and-gaussian-noise-5.2"><span class="toc-item-num">5.2&nbsp;&nbsp;</span>L1 reg and gaussian noise</a></span></li><li><span><a href="#Model-with-Dropout" data-toc-modified-id="Model-with-Dropout-5.3"><span class="toc-item-num">5.3&nbsp;&nbsp;</span>Model with Dropout</a></span></li></ul></li><li><span><a href="#initial-DNN-Training-runs-(data-not-corrected-for-zeroes)" data-toc-modified-id="initial-DNN-Training-runs-(data-not-corrected-for-zeroes)-6"><span class="toc-item-num">6&nbsp;&nbsp;</span>initial DNN Training runs (data not corrected for zeroes)</a></span><ul class="toc-item"><li><span><a href="#Andi's-initial-DNN-using-gn" data-toc-modified-id="Andi's-initial-DNN-using-gn-6.1"><span class="toc-item-num">6.1&nbsp;&nbsp;</span>Andi's initial DNN using gn</a></span></li><li><span><a href="#without-any-regularization" data-toc-modified-id="without-any-regularization-6.2"><span class="toc-item-num">6.2&nbsp;&nbsp;</span>without any regularization</a></span><ul class="toc-item"><li><span><a href="#Investigation-of-model-performance-errors" data-toc-modified-id="Investigation-of-model-performance-errors-6.2.1"><span class="toc-item-num">6.2.1&nbsp;&nbsp;</span>Investigation of model performance errors</a></span></li><li><span><a href="#outlier-investigation-after-answer-by-Jochem/Pavle" data-toc-modified-id="outlier-investigation-after-answer-by-Jochem/Pavle-6.2.2"><span class="toc-item-num">6.2.2&nbsp;&nbsp;</span>outlier investigation after answer by Jochem/Pavle</a></span></li></ul></li><li><span><a href="#Trying-to-reproduce-best-hyperscan-run" data-toc-modified-id="Trying-to-reproduce-best-hyperscan-run-6.3"><span class="toc-item-num">6.3&nbsp;&nbsp;</span>Trying to reproduce best hyperscan run</a></span></li><li><span><a href="#Try-out-DNN-with-dropout" data-toc-modified-id="Try-out-DNN-with-dropout-6.4"><span class="toc-item-num">6.4&nbsp;&nbsp;</span>Try out DNN with dropout</a></span></li></ul></li><li><span><a href="#Hyperparameter-scans-(data-not-corrected-for-zeroes)" data-toc-modified-id="Hyperparameter-scans-(data-not-corrected-for-zeroes)-7"><span class="toc-item-num">7&nbsp;&nbsp;</span>Hyperparameter scans (data not corrected for zeroes)</a></span><ul class="toc-item"><li><span><a href="#Test:-Make-a-hyperparameter-scan-A" data-toc-modified-id="Test:-Make-a-hyperparameter-scan-A-7.1"><span class="toc-item-num">7.1&nbsp;&nbsp;</span>Test: Make a hyperparameter scan A</a></span></li><li><span><a href="#Offline-batch-parameter-scan" data-toc-modified-id="Offline-batch-parameter-scan-7.2"><span class="toc-item-num">7.2&nbsp;&nbsp;</span>Offline batch parameter scan</a></span></li><li><span><a href="#TODO-ModelB" data-toc-modified-id="TODO-ModelB-7.3"><span class="toc-item-num">7.3&nbsp;&nbsp;</span>TODO ModelB</a></span></li><li><span><a href="#TODO:-Model-C:-scan-regulatisation-and-noise" data-toc-modified-id="TODO:-Model-C:-scan-regulatisation-and-noise-7.4"><span class="toc-item-num">7.4&nbsp;&nbsp;</span>TODO: Model C: scan regulatisation and noise</a></span></li></ul></li><li><span><a href="#DNN-runs-with-data-set-cleaned-for-zero-energy-measurements" data-toc-modified-id="DNN-runs-with-data-set-cleaned-for-zero-energy-measurements-8"><span class="toc-item-num">8&nbsp;&nbsp;</span>DNN runs with data set cleaned for zero energy measurements</a></span><ul class="toc-item"><li><span><a href="#without-any-regularization" data-toc-modified-id="without-any-regularization-8.1"><span class="toc-item-num">8.1&nbsp;&nbsp;</span>without any regularization</a></span></li></ul></li><li><span><a href="#SVM-to-see-what-a-linear-model-can-do" data-toc-modified-id="SVM-to-see-what-a-linear-model-can-do-9"><span class="toc-item-num">9&nbsp;&nbsp;</span>SVM to see what a linear model can do</a></span></li><li><span><a href="#Test-for-leaving-out-some-files-from-the-training-set" data-toc-modified-id="Test-for-leaving-out-some-files-from-the-training-set-10"><span class="toc-item-num">10&nbsp;&nbsp;</span>Test for leaving out some files from the training set</a></span></li><li><span><a href="#new-data-splitting-in-respect-to-separate-files" data-toc-modified-id="new-data-splitting-in-respect-to-separate-files-11"><span class="toc-item-num">11&nbsp;&nbsp;</span>new data splitting in respect to separate files</a></span><ul class="toc-item"><li><span><a href="#tests-using-mean-values-per-file" data-toc-modified-id="tests-using-mean-values-per-file-11.1"><span class="toc-item-num">11.1&nbsp;&nbsp;</span>tests using mean values per file</a></span><ul class="toc-item"><li><span><a href="#DNN-toy-run" data-toc-modified-id="DNN-toy-run-11.1.1"><span class="toc-item-num">11.1.1&nbsp;&nbsp;</span>DNN toy run</a></span></li><li><span><a href="#l1-reg" data-toc-modified-id="l1-reg-11.1.2"><span class="toc-item-num">11.1.2&nbsp;&nbsp;</span>l1 reg</a></span></li></ul></li><li><span><a href="#using-full-data-set,-but-validation-split-off-according-to-files" data-toc-modified-id="using-full-data-set,-but-validation-split-off-according-to-files-11.2"><span class="toc-item-num">11.2&nbsp;&nbsp;</span>using full data set, but validation split off according to files</a></span><ul class="toc-item"><li><span><a href="#original-initial-DNN" data-toc-modified-id="original-initial-DNN-11.2.1"><span class="toc-item-num">11.2.1&nbsp;&nbsp;</span>original initial DNN</a></span></li><li><span><a href="#l1-reg" data-toc-modified-id="l1-reg-11.2.2"><span class="toc-item-num">11.2.2&nbsp;&nbsp;</span>l1 reg</a></span></li></ul></li></ul></li><li><span><a href="#physics-based-model" data-toc-modified-id="physics-based-model-12"><span class="toc-item-num">12&nbsp;&nbsp;</span>physics based model</a></span><ul class="toc-item"><li><span><a href="#Xe-data-on-cross-section,-charge-vs-photon-energy" data-toc-modified-id="Xe-data-on-cross-section,-charge-vs-photon-energy-12.1"><span class="toc-item-num">12.1&nbsp;&nbsp;</span>Xe data on cross section, charge vs photon energy</a></span></li></ul></li></ul></div>

# +
import os
import re
import json
import array
import random
import pickle
import time
from math import sqrt

import numpy as np
import pandas as pd
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats
import tensorflow
import keras 
from keras.models import Sequential, Model,load_model
from keras.layers import Input, Dense, Conv1D, Dropout, Activation, GaussianNoise
from keras import regularizers
from sklearn.model_selection import train_test_split
from sklearn import metrics, svm
from sklearn.metrics import mean_squared_error, mean_absolute_error
from IPython.display import clear_output
from sklearn.preprocessing import RobustScaler,MinMaxScaler
from keras.optimizers import SGD
import talos as ta
#matplotlib inline
# -

import itertools
import glob

# # Configuration

#topdir = "/psi/home/adelmann/data/ml-gasmon/"
topdir = "/psi/home/feichtinger/jupyterhub/ml-gasmon"
directory = os.path.join(topdir, "cleaned/")
xlsxFn    = os.path.join('XeX77.xlsx')


# # Support Routines

def print_model_err(model, xt, yt):
    y_pred=model.predict(xt)
    print('Mean Absolute Error:', metrics.mean_absolute_error(yt, y_pred))
    print('Mean Squared Error:', metrics.mean_squared_error(yt, y_pred))
    print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(yt, y_pred)))


# ## Visualization

# +
def calc_gaussian_kde(x, y):
    xy = np.vstack((x, y))
    f = scipy.stats.gaussian_kde(xy)
    return f(xy)

def densitycolor(x, y):
    # calculate point density
    density = calc_gaussian_kde(x, y)
    # sort points according to density values
    indices = density.argsort()
    return x[indices], y[indices], density[indices]

def coloredscatter(x, y, alpha=1.0, cmap="Blues", ax=None):
    if ax is None:
        fig, ax = plt.subplots()

    x, y, z = densitycolor(x, y)
    paths = ax.scatter(x, y, c=z, s=50, edgecolor='', alpha=alpha, cmap=cmap)
    plt.colorbar(paths, ax=ax)


# +
#found online for axis formatting
import matplotlib.ticker as mticker

class MathTextSciFormatter(mticker.Formatter):
    def __init__(self, fmt="%1.2e"):
        self.fmt = fmt
    def __call__(self, x, pos=None):
        s = self.fmt % x
        decimal_point = '.'
        positive_sign = '+'
        tup = s.split('e')
        significand = tup[0].rstrip(decimal_point)
        sign = tup[1][0].replace(positive_sign, '')
        exponent = tup[1][1:].lstrip('0')
        if exponent:
            exponent = '10^{%s%s}' % (sign, exponent)
        if significand and exponent:
            s =  r'%s{\times}%s' % (significand, exponent)
        else:
            s =  r'%s%s' % (significand, exponent)
        return "${}$".format(s)
    
# define class for showing training plot - found online
class PlotLosses(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.i = 0
        self.x = []
        self.losses = []
        self.val_losses = []
        
        self.fig = plt.figure()
        
        self.logs = []

    def on_epoch_end(self, epoch, logs={}):
        
        self.logs.append(logs)
        self.x.append(self.i)
        self.losses.append(logs.get('loss'))
        self.val_losses.append(logs.get('val_loss'))
        self.i += 1
        
        clear_output(wait=True)
        plt.plot(self.x, self.losses, label="loss")
        plt.plot(self.x, self.val_losses, label="val_loss")
        plt.legend()
        plt.yscale('log')
        plt.ylabel('error')
        plt.xlabel('epoch')
        plt.show();
        
plot_losses = PlotLosses()


class PlotAccuracy(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.i = 0
        self.x = []
        self.acc = []
        self.val_acc = []
        
        self.fig = plt.figure()
        
        self.logs = []

    def on_epoch_end(self, epoch, logs={}):
        
        self.logs.append(logs)
        self.x.append(self.i)
        self.acc.append(logs.get('acc'))
        self.val_acc.append(logs.get('val_acc'))
        self.i += 1
        
        clear_output(wait=True)
        plt.plot(self.x, self.acc, label="accuracy")
        plt.plot(self.x, self.val_acc, label="val_accuracy")
        plt.legend()
        plt.ylabel('error')
        plt.xlabel('epoch')
        plt.show();

plot_accuracy = PlotAccuracy()

def plotModelPerf2(m, modelDesc, X_RS, y_RSscaled, y_var_list, transformer_y, figDir, alpha=0.05):

    colNames = y_var_list.append(pd.Index([modelDesc]))
    errDf = pd.DataFrame(columns=colNames)                           
    Y_pred5 = m.predict(X_RS)
    Y_pred5_inv = transformer_y.inverse_transform(Y_pred5)
    
    y_RS = transformer_y.inverse_transform(y_RSscaled)
    
    #fig.subplots_adjust(wspace=1.,hspace=1.)
    err = []
    for i in range(0,y_RS.shape[1]):
        fig = plt.figure(figsize=(8,8))
        #ax = fig.add_subplot(6, 2, i+1)
        indx = np.argsort(y_RS[:,i])    # y_RS_inv
        plt.plot(Y_pred5_inv[indx,i],y_RS[indx,i], marker='o', linestyle='',alpha=alpha)
        plt.plot(y_RS[indx,i],y_RS[indx,i])
        
        plt.ylabel(y_var_list[i] + ' of Random Sample')
        plt.xlabel(y_var_list[i] + ' of Surrogate Model')
        plt.savefig(figDir + y_var_list[i] + '.png')
        
        merr = metrics.mean_absolute_error(Y_pred5_inv[indx,i],y_RS[indx,i])
        
        plt.title(modelDesc + ' MAE= ' + "{:.3e}".format(merr), fontsize=8)
        err.append(merr)
    err.append(modelDesc)
    errDf.loc[len(errDf)] = err
    return errDf

def plotModelPerf2dens(m, modelDesc, X_RS, y_RSscaled, y_var_list, transformer_y, figDir, cmap='Blues'):

    colNames = y_var_list.append(pd.Index([modelDesc]))
    errDf = pd.DataFrame(columns=colNames)                           
    Y_pred5 = m.predict(X_RS)
    Y_pred5_inv = transformer_y.inverse_transform(Y_pred5)
    
    y_RS = transformer_y.inverse_transform(y_RSscaled)
    
    #fig.subplots_adjust(wspace=1.,hspace=1.)
    err = []
    for i in range(0,y_RS.shape[1]):
        fig = plt.figure(figsize=(8,8))
        #ax = fig.add_subplot(6, 2, i+1)
        indx = np.argsort(y_RS[:,i])    # y_RS_inv
        #plt.plot(Y_pred5_inv[indx,i],y_RS[indx,i], marker='o', linestyle='')
        coloredscatter(Y_pred5_inv[indx,i],y_RS[indx,i])
        plt.plot(y_RS[indx,i],y_RS[indx,i])
        
        plt.ylabel(y_var_list[i] + ' of Random Sample')
        plt.xlabel(y_var_list[i] + ' of Surrogate Model')
        plt.savefig(figDir + y_var_list[i] + '.png')
        
        merr = metrics.mean_absolute_error(Y_pred5_inv[indx,i],y_RS[indx,i])
        
        plt.title(modelDesc + ' MAE= ' + "{:.3e}".format(merr), fontsize=8)
        err.append(merr)
    err.append(modelDesc)
    errDf.loc[len(errDf)] = err
    return errDf


# -

# # Dataset creation

# ## Dataset reading and preprocessing definition

def makeDataSetInterpolated(directory, excelFn, doInterpolate=True, dropBadPulses=True, verbose=False,
                           CALCTthreshold=-50, CALCSthreshold=-50):
    first = True
    data = []
    for filename in sorted(os.listdir(directory)):
        if filename.endswith(".csv"):
            fntmp = re.sub(r'.*dp', '', filename)
            expNumber = re.sub(r'-nomeans.csv', '', fntmp)
            file_excel = pd.read_excel(excelFn)
            multVoltag = file_excel.iloc[int(expNumber)]['XeMultVoltag'] 
            try:
                dp  = pd.read_csv(directory+filename, sep=";") 
            except:
                print ("Can not read " + directory + filename)
                continue
                
            dp = dp[['SARFE10-PBIG050-EVR0:CALCT.value', 
                    'SARFE10-PBIG050-EVR0:CALCS.value',
                    'SARFE10-PSSS059:SPECTRUM_CENTER.value',
                    'SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.value']]
            dp.columns = ['CALCT','CALCS','SPECTRUM_CENTER','PHOTON-ENERGY-PER-PULSE']

            if doInterpolate:
                dp['PHOTON-ENERGY-PER-PULSE'].interpolate(method='linear', 
                                                              inplace=True, 
                                                              limit_direction='forward', 
                                                              axis=0)
            
            dp = dp.dropna();

            # condition for bad pulse
            if dropBadPulses:
                validT = dp['CALCT'] < CALCTthreshold
                validS = dp['CALCS'] < CALCSthreshold
                dp = dp[validT & validS]
            
            dp['XeMultVoltag'] = multVoltag
            dp['rawDataFile']  = filename
            
            if first:
                data = dp
                first = False
            else:
                data = data.append(dp,ignore_index=True)

            if verbose:
                print("Datapoint", expNumber, "gave", len(dp), "values")
    data.reset_index(inplace=True)
    data.dropna()
    return data

# The initial DNN regression runs led to the identification of measurement artifacts where the measured average pulse energy was stored as zero, heavily influencing the interpolated pulse energies in their neighborhood.
#
# In the following data cleaning, the zeros are replaced by NaN and all interpolation is done based on the two surrounding values. The correctness of this procedure is unclear, since if there really was no beam in the vicinity of this measurement one maybe should rather not use the points of those regions at all

def makeDataSetInterpolated2(directory, excelFn, doInterpolate=True, dropBadPulses=True, verbose=False,
                           CALCTthreshold=-50, CALCSthreshold=-50):
    first = True
    data = pd.DataFrame()
    for filename in sorted(glob.glob(os.path.join(directory,'dp*-nomeans.csv'))):
        mat = re.match('^dp(\d+).*', os.path.basename(filename))
        if mat is None:
            print(f"WARNING: Could not parse number part of filename {os.path.basename(filename)}")
            continue
        expNumber = int(mat.group(1))
        file_excel = pd.read_excel(excelFn)
        multVoltag = file_excel.iloc[expNumber]['XeMultVoltag'] 
        try:
            dp  = pd.read_csv(filename, sep=";") 
        except:
            print (f"WARNING: Can not read {os.path.basename(filename)}. skipping...")
            continue

        dp = dp[['SARFE10-PBIG050-EVR0:CALCT.value', 
                'SARFE10-PBIG050-EVR0:CALCS.value',
                'SARFE10-PSSS059:SPECTRUM_CENTER.value',
                'SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.value']]
        dp.columns = ['CALCT','CALCS','SPECTRUM_CENTER','PHOTON-ENERGY-PER-PULSE']

        # Replace zeroes by NaN
        fzeroes = dp.loc[dp['PHOTON-ENERGY-PER-PULSE'] == 0.0]
        if fzeroes.shape[0] > 0:
            print(f"zeroes found in PEPP-avg of {os.path.basename(filename)} at {fzeroes.index.values}")
            dp.loc[dp['PHOTON-ENERGY-PER-PULSE'] == 0.0, 'PHOTON-ENERGY-PER-PULSE'] = np.NaN

        if doInterpolate:
            dp['PHOTON-ENERGY-PER-PULSE'].interpolate(method='linear', 
                                                          inplace=True, 
                                                          limit_direction='forward', 
                                                          axis=0)

        dp = dp.dropna();

        # condition for bad pulse
        if dropBadPulses:
            dp = dp.query('CALCT < @CALCTthreshold & CALCS < @CALCSthreshold')

        dp['XeMultVoltag'] = multVoltag
        dp['rawDataFile']  = os.path.basename(filename)
        
        data = pd.concat([data,dp])

        if verbose:
            print("Datapoint", expNumber, "gave", len(dp), "values")

    data.dropna()
    data.reset_index(inplace=True)
    return data

# ## Make dataset 
#
# (Can not read /psi/home/adelmann/SwissFEL-Gas-1/cleaned/dp41-nomeans.csv is ok)

data      = makeDataSetInterpolated2(directory,xlsxFn,CALCTthreshold=-50,CALCSthreshold=-50,verbose=False)

data.info()

data.describe()

data.tail()

#nr files actually used
data['rawDataFile'].nunique()

# Some data files contain almost no measurement points

fig, ax = plt.subplots(figsize=(16,5))
data['rawDataFile'].value_counts().sort_index().plot(kind='bar', ax=ax)
tmp = ax.set_xticklabels(ax.get_xticklabels(),rotation=45,horizontalalignment='right')


# ## Original Training/Test Split: Random over all samples

var_indep = ['SPECTRUM_CENTER','XeMultVoltag','CALCT','CALCS']
var_dep = ['PHOTON-ENERGY-PER-PULSE']

# +
train, validate, test = np.split(data.sample(frac=1), [int(.6*len(data)), int(.8*len(data))])

x_train    = train[var_indep]
y_train    = train[var_dep]
x_validate = validate[var_indep]
y_validate = validate[var_dep]
x_test     = test[var_indep]
y_test     = test[var_dep]
# -

# ## Data scaling for DNN training

# Make a scaler and scale to -1 ... 1

transformer_x = 0
transformer_y = 0


transformer_x = MinMaxScaler(feature_range=(-1, 1)).fit(data[var_indep].values)
transformer_y = MinMaxScaler(feature_range=(-1, 1)).fit(data[var_dep].values)

# +
x_train    = transformer_x.transform(x_train)
x_validate = transformer_x.transform(x_validate)
x_test     = transformer_x.transform(x_test)

y_train    = transformer_y.transform(y_train)
y_validate = transformer_y.transform(y_validate)
y_test     = transformer_y.transform(y_test)
# -
# # Examining data

# Information on the variables from Pavle Juranic:
#    - **CALCS and CALCT** are actually in uJ, or pulse energy.  The actual
#      waveform we have is Vs, since it's an integral of a waveform, but
#      the end value that we wish to get is uJ, which it is directly
#      related to.
#    - The **photon-energy-per-pulse-avg** is also in uJ, but after it's
#      been averaged over about 30 seconds and divided by the repetition
#      rate of the machine.
#    - **Spectrum-center** is in eV.
#    - Energy (the FEL energy) is also in eV.  (**XeMultVoltag** ???)
#

# Typical values?
#
# XePhotEnergyL = 6000.     # Ev
# XePhotEnergyH = 12500.
#
# XeMultVoltagL = 600.      # V
# XeMultVoltagH = 1600.
#
# XePulseEnergL = 10        # uJ
# XePulseEnergH = 500

sns.pairplot(data[['CALCT', 'CALCS', 'SPECTRUM_CENTER','XeMultVoltag','PHOTON-ENERGY-PER-PULSE']])

# Just plotting the data from a few files shows that within the files the data have very little variation as compared to between files.

# +
fchoice = "01,02,03,05,08,09,10,35,36,64,65,66,76"
colname = ["PHOTON-ENERGY-PER-PULSE","XeMultVoltag","SPECTRUM_CENTER","CALCS","CALCT"]
flist = [f'rawDataFile == "dp{fnum}-nomeans.csv"' for fnum in fchoice.split(",")]

tmpdata = data.query(" | ".join(flist)) \
                        .drop(labels="index", axis=1) \
                        .reset_index(drop=True).reset_index()
for col in colname:
    sns.lmplot(x="index", y=col, hue="rawDataFile",
              data=tmpdata,
              fit_reg=False, size=4, aspect=3)

# -

# Rendering the variables for all files and samples

#sns.lmplot(data=data.drop(labels='index',axis=1).reset_index(drop=True).reset_index(),
#           x='index',y='PHOTON-ENERGY-PER-PULSE', hue='rawDataFile',
#          fit_reg=False)
fig = plt.figure(figsize=(16,36))
for num,col in enumerate(colname):
    ax = fig.add_subplot(len(colname),1,num+1)
    for fname in sorted(data.rawDataFile.unique()):
        data.query('rawDataFile == @fname')[col] \
            .plot(marker='o', linestyle='',ax=ax)
    ax.set_xlabel('sample index')
    ax.set_ylabel(col)
fig.tight_layout()

# Let's identify files with high variation in the target PEPP variable. Can we see some patterns in the other variables for these files (e.g. a more strongly varying CALCS/CALCT, since the other variables are basically constant).

# +
# get files list with high variation in PEPP
flist = data.groupby('rawDataFile') \
    .agg({'PHOTON-ENERGY-PER-PULSE': ['min','max']})['PHOTON-ENERGY-PER-PULSE'] \
    .assign(diff = lambda x: x['max'] - x['min']) \
    .query('diff > 30').index.to_list()

flist = [f'rawDataFile == "{fnum}"' for fnum in flist]

tmpdata = data.query(" | ".join(flist)) \
                        .drop(labels="index", axis=1) \
                        .reset_index(drop=True).reset_index()

colname = ["PHOTON-ENERGY-PER-PULSE","XeMultVoltag","SPECTRUM_CENTER","CALCS","CALCT"]
for col in colname:
    sns.lmplot(x="index", y=col, hue="rawDataFile",
              data=tmpdata,
               palette=sns.color_palette("Set1", len(flist)),
              fit_reg=False, size=4, aspect=3)

# -

# In the PEPP vs CALCT plot the points of PEPP ~ 200 stick out and seem to disturb the
# general pattern exhibited by the rest of the data. The variation of the CALCT values also is greatest, there. Let's have a closer look.

#data.plot(x='PHOTON-ENERGY-PER-PULSE', y='CALCT', marker='.', linestyle='')
sns.lmplot(x="PHOTON-ENERGY-PER-PULSE", y='CALCT', hue="rawDataFile",
              data=data,
               palette=sns.color_palette("Set1", len(flist)),
              fit_reg=False, size=4, aspect=3, legend=False)


sns.lmplot(x="PEPP", y='CALCT', hue="rawDataFile",
              data=data.rename(columns = {'PHOTON-ENERGY-PER-PULSE': 'PEPP'}) \
                        .query('180 < PEPP < 250'),
               palette=sns.color_palette("Set1", len(flist)),
              fit_reg=False, size=4, aspect=3, legend=True)


# So, the points are all part of file dp05

# ## Observations from data set inspection
#    * data have very little variation within each file as compared to between files (as explained by the experimental setup).
#    * **XeMultVoltag**, **Spectrum_Center** show no and almost no variation within one file.
#    * The interpolated target value **PEPP** (photon energy per pulse) varies just slightly within files, with a few files showing some moderate variation.
#    * **Spectrum_Center** values grow monotonically from the first to the last file.
#       * according to the experimental setup. 
#    * **CALCS and CALCT** are strongly correlated (q.v. correlation plot further below). They show the biggest relative variations of all variables within one file.

# # DNN Model definitions

# ## L2reg and gaussian noise 

# +
def build_ff_mdl_small(in_dim = 2, out_dim = 1, l1 = 8, l2 = 6, l3 = 4, l4= 4, 
                       opt = 'adam', loss = 'mse', act = 'tanh', l2reg = 0.00, gn=0.001):
    layer_0 = Input(shape=(in_dim,))
    layer_1 = Dense(l1, activation=act, activity_regularizer=regularizers.l2(l2reg))(layer_0)
    layer_1 = GaussianNoise(gn)(layer_1)
    layer_2 = Dense(l2, activation=act, activity_regularizer=regularizers.l2(l2reg))(layer_1)
    layer_2=GaussianNoise(gn)(layer_2)
    layer_3 = Dense(l3, activation=act, activity_regularizer=regularizers.l2(l2reg))(layer_2)
    layer_4 = Dense(l4, activation=act, activity_regularizer=regularizers.l2(l2reg))(layer_3)
    layer_4=GaussianNoise(gn)(layer_4)
    layer_5 = Dense(out_dim, activation='linear')(layer_4)
    model = Model(inputs=layer_0, outputs=layer_5)
    model.compile(loss=loss, optimizer=opt, metrics=['mse'])
    return model


def build_ff_mdl_smallnn(in_dim = 2, out_dim = 1, l1 = 8, l2 = 6, l3 = 4, l4= 4, 
                         opt = 'adam', loss = 'mse', act = 'tanh', l2reg = 0.00):
    layer_0 = Input(shape=(in_dim,))
    layer_1 = Dense(l1, activation=act, activity_regularizer=regularizers.l2(l2reg))(layer_0)
    layer_2 = Dense(l2, activation=act, activity_regularizer=regularizers.l2(l2reg))(layer_1)
    layer_3 = Dense(l3, activation=act, activity_regularizer=regularizers.l2(l2reg))(layer_2)
    layer_4 = Dense(l4, activation=act, activity_regularizer=regularizers.l2(l2reg))(layer_3)
    layer_5 = Dense(out_dim, activation='linear')(layer_4)
    model = Model(inputs=layer_0, outputs=layer_5)
    model.compile(loss=loss, optimizer=opt, metrics=['mse'])
    return model




# +
def build_ff_mdl_smallA(params, in_dim = 2, out_dim = 1, 
                       l1 = 8, l2 = 6, l3 = 4, l4= 4, opt = 'adam', loss = 'mse', l2reg = 0.00, gn=0.001):
    layer_0 = Input(shape=(in_dim,))
    layer_1 = Dense(l1, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_0)
    layer_1 = GaussianNoise(gn)(layer_1)
    layer_2 = Dense(l2, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_1)
    layer_2 = GaussianNoise(gn)(layer_2)
    layer_3 = Dense(l3, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_2)
    layer_4 = Dense(l4, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_3)
    layer_4 = GaussianNoise(gn)(layer_4)
    layer_5 = Dense(out_dim, activation='linear')(layer_4)
    model = Model(inputs=layer_0, outputs=layer_5)
    model.compile(loss=loss, optimizer=opt, metrics=['mse'])
    return model

def modelA(x_train, y_train, x_val, y_val, params):
    
    model = build_ff_mdl_smallA(params,
                               in_dim  = x_train.shape[1], 
                               out_dim = y_train.shape[1], 
                               l1 = params['mult_neuron']*8, l2 = params['mult_neuron']*6, 
                               l3 = params['mult_neuron']*4, l4 = params['mult_neuron']*4)
    
    # make sure history object is returned by model.fit()
    out = model.fit(x=x_train, 
                    y=y_train,
                    validation_data=[x_val, y_val],
                    epochs=2000, shuffle='true',
                    batch_size=params['batch_size'],
                    verbose='false') # callbacks=[plot_losses]
    
    # modify the output model
    return out, model


# +
def build_ff_mdl_smallB(params, in_dim = 2, out_dim = 1, 
                       l1 = 8, l2 = 6, l3 = 4, l4= 4, opt = 'adam', loss = 'mse', l2reg = 0.00, gn=0.001):
    layer_0 = Input(shape=(in_dim,))
    layer_1 = Dense(l1, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_0)
    layer_1 = GaussianNoise(gn)(layer_1)
    layer_2 = Dense(l2, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_1)
    layer_2 = GaussianNoise(gn)(layer_2)
    layer_3 = Dense(l3, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_2)
    layer_4 = Dense(l4, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_3)
    layer_4 = GaussianNoise(gn)(layer_4)
    layer_5 = Dense(out_dim, activation='linear')(layer_4)
    model = Model(inputs=layer_0, outputs=layer_5)
    model.compile(loss=loss, optimizer=opt, metrics=['mse'])
    return model

def modelB(x_train, y_train, x_val, y_val, params):
    
    mB = build_ff_mdl_smallB(params, in_dim = x_train.shape[1], out_dim = y_train.shape[1],
                             l1 = params['mult_neuron']*8, l2 = params['mult_neuron']*6, 
                             l3 = params['mult_neuron']*4, l4 = params['mult_neuron']*4)
    
    
    es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, 
                                       patience=100)

    # make sure history object is returned by model.fit()
    out = mB.fit(x=x_train, 
                    y=y_train,
                    validation_data=[x_val, y_val],
                    epochs=2000, shuffle='true',
                    batch_size=params['batch_size'],
                    verbose='false') #, callbacks=[es])
    
    # modify the output model
    return out, mB


# +
def build_ff_mdl_smallC(params, in_dim = 2, out_dim = 1, 
                       l1 = 8, l2 = 6, l3 = 4, l4= 4, opt = 'adam', loss = 'mse', l2reg = 0.00, gn=0.001):
    layer_0 = Input(shape=(in_dim,))
    layer_1 = Dense(l1, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_0)
    layer_1 = GaussianNoise(gn)(layer_1)
    layer_2 = Dense(l2, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_1)
    layer_2 = GaussianNoise(gn)(layer_2)
    layer_3 = Dense(l3, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_2)
    layer_4 = Dense(l4, activation=params['activation'], 
                    activity_regularizer=regularizers.l2(l2reg))(layer_3)
    layer_4 = GaussianNoise(gn)(layer_4)
    layer_5 = Dense(out_dim, activation='linear')(layer_4)
    model = Model(inputs=layer_0, outputs=layer_5)
    model.compile(loss=loss, optimizer=opt, metrics=['mse'])
    return model

def modelC(x_train, y_train, x_val, y_val, params):
    
    mC = build_ff_mdl_smallB(params, in_dim = x_train.shape[1], out_dim = y_train.shape[1],
                             l1 = params['mult_neuron']*8, l2 = params['mult_neuron']*6, 
                             l3 = params['mult_neuron']*4, l4 = params['mult_neuron']*4,
                             l2reg = params['l2reg'], gn = params['noise'])
    
    
    es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=1000)

    # make sure history object is returned by model.fit()
    out = mC.fit(x=x_train, 
                    y=y_train,
                    validation_data=[x_val, y_val],
                    epochs=2000, shuffle='true',
                    batch_size=params['batch_size'],
                    verbose='false', callbacks=[es])
    
    # modify the output model
    return out, mC


# -
# ## L1 reg and gaussian noise

# +
def build_dnn_l1_small(in_dim = 2, out_dim = 1, l1 = 8, l2 = 6, l3 = 4, l4= 4, 
                       opt = 'adam', loss = 'mse', act = 'tanh', l1reg = 0.00):
    layer_0 = Input(shape=(in_dim,))
    layer_1 = Dense(l1, activation=act, activity_regularizer=regularizers.l1(l1reg))(layer_0)
    layer_2 = Dense(l2, activation=act, activity_regularizer=regularizers.l1(l1reg))(layer_1)
    layer_3 = Dense(l3, activation=act, activity_regularizer=regularizers.l1(l1reg))(layer_2)
    layer_4 = Dense(l4, activation=act, activity_regularizer=regularizers.l1(l1reg))(layer_3)
    layer_5 = Dense(out_dim, activation='linear')(layer_4)
    model = Model(inputs=layer_0, outputs=layer_5)
    model.compile(loss=loss, optimizer=opt, metrics=['mse'])
    return model

def build_dnn_l1_gn_small(in_dim = 2, out_dim = 1, l1 = 8, l2 = 6, l3 = 4, l4= 4, 
                       opt = 'adam', loss = 'mse', act = 'tanh', l1reg = 0.00, gn=0.001):
    layer_0 = Input(shape=(in_dim,))
    layer_1 = Dense(l1, activation=act, activity_regularizer=regularizers.l1(l1reg))(layer_0)
    layer_1 = GaussianNoise(gn)(layer_1)
    layer_2 = Dense(l2, activation=act, activity_regularizer=regularizers.l1(l1reg))(layer_1)
    layer_2=GaussianNoise(gn)(layer_2)
    layer_3 = Dense(l3, activation=act, activity_regularizer=regularizers.l1(l1reg))(layer_2)
    layer_4 = Dense(l4, activation=act, activity_regularizer=regularizers.l1(l1reg))(layer_3)
    layer_4=GaussianNoise(gn)(layer_4)
    layer_5 = Dense(out_dim, activation='linear')(layer_4)
    model = Model(inputs=layer_0, outputs=layer_5)
    model.compile(loss=loss, optimizer=opt, metrics=['mse'])
    return model



# -

# ## Model with Dropout

def build_ff_mdl_smallDropOut(in_dim = 2, out_dim = 1, l1 = 8, l2 = 6, l3 = 4, l4= 4, 
                       opt = 'adam', loss = 'mse', act = 'tanh', dout=0.2):
    layer_0 = Input(shape=(in_dim,))
    layer_1 = Dense(l1, activation=act)(layer_0)
    layer_1 = Dropout(dout)(layer_1)
    layer_2 = Dense(l2, activation=act)(layer_1)
    layer_2 = Dropout(dout)(layer_2)
    layer_3 = Dense(l3, activation=act)(layer_2)
    layer_3 = Dropout(dout)(layer_3)
    layer_4 = Dense(l4, activation=act)(layer_3)
    layer_4 = Dropout(dout)(layer_4)
    layer_5 = Dense(out_dim, activation='linear')(layer_4)
    model = Model(inputs=layer_0, outputs=layer_5)
    model.compile(loss=loss, optimizer=opt, metrics=['mse'])
    return model


# # initial DNN Training runs (data not corrected for zeroes)

# ## Andi's initial DNN using gn

# If run to 2000 iterations, Andi's DNN with minor gn of 0.001 and l2reg=0 is actually already among the best that were tested in the initial parameter scans.

# %%time
model_0 = build_ff_mdl_small(in_dim  = x_train.shape[1], 
                             out_dim = y_train.shape[1])
#mc = keras.callbacks.ModelCheckpoint('best_model_1.h5', monitor='val_loss', mode='min', save_best_only=True)
es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0 = model_0.fit(x=x_train, y=y_train, 
                     validation_data=(x_validate,y_validate),
                     batch_size=250, shuffle='true',epochs=100,  #2000, 
                     verbose='false', callbacks=[plot_losses,es])

print_model_err(model_0, x_test, y_test)

# %%time
#plotModelPerf2dens(model_0, 'Model 0', x_test[0:1000,:], y_test[0:1000,:], test[['PHOTON-ENERGY-PER-PULSE']].columns, transformer_y, '.')
plotModelPerf2dens(model_0, 'Model 0', x_test, y_test, test[['PHOTON-ENERGY-PER-PULSE']].columns, transformer_y, '.')

# ## without any regularization

# The model without any regularization already is able to yield a quite good fit. There is only slight overfitting based on the loss plot, below

# %%time
model_0_noreg = build_ff_mdl_small(in_dim  = x_train.shape[1], out_dim = y_train.shape[1],
                                  l2reg=0.0, gn=0.0)
#mc = keras.callbacks.ModelCheckpoint('best_model_1.h5', monitor='val_loss', mode='min', save_best_only=True)
es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0_noreg = model_0_noreg.fit(x=x_train, y=y_train, 
                     validation_data=(x_validate,y_validate),
                     batch_size=250, shuffle='true',epochs=2000, 
                     verbose='false', callbacks=[plot_losses,es])

print_model_err(model_0_noreg, x_test, y_test)

# %%time
plotModelPerf2(model_0_noreg, 'Model0 No Regularization', x_test, y_test, test[var_dep].columns, transformer_y, '.')

# %%time
model_0_noreg_m4 = build_ff_mdl_small(in_dim  = x_train.shape[1], out_dim = y_train.shape[1],
                                      l1 = 4*8, l2 = 4*6, l3 = 4*4, l4= 4*4,
                                  l2reg=0.0, gn=0.0)
#mc = keras.callbacks.ModelCheckpoint('best_model_1.h5', monitor='val_loss', mode='min', save_best_only=True)
es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0_noreg_m4 = model_0_noreg_m4.fit(x=x_train, y=y_train, 
                     validation_data=(x_validate,y_validate),
                     batch_size=250, shuffle='true',epochs=2000, 
                     verbose='false', callbacks=[plot_losses,es])

model_0_noreg_m4.save('model_0_noreg_m4.h5')

pd.DataFrame(hist_0_noreg_m4.history).to_csv('model_0_noreg_m4-hist.csv')

print_model_err(model_0_noreg_m4, x_test, y_test)

model_0_noreg_m4 = keras.models.load_model('model_0_noreg_m4.h5')

# %%time
plotModelPerf2(model_0_noreg_m4, 'Model0 m4 No Regularization', x_test, y_test, test[var_dep].columns, transformer_y, '.')

# ### Investigation of model performance errors
# The biggest departures of the predicted energy from the ground truth energies happen in series at certain discrete values of the predicted energy. Try to get some information about this systematic behavior.

y_predict = transformer_y.inverse_transform(model_0_noreg_m4.predict(x_test))

# Use the original data (`test`) that contains the untransformed values and also the file names.

y_predict

test['PHOTON-ENERGY-PER-PULSE'].values[np.newaxis].transpose()

#  %matplotlib notebook
fig,ax = plt.subplots(figsize=(15,10))
ax.plot(y_predict, test['PHOTON-ENERGY-PER-PULSE'], marker='o', linestyle='', alpha=0.1)
# the colorplot makes the kernel die
# coloredscatter(y_predict, test['PHOTON-ENERGY-PER-PULSE'].values[np.newaxis].transpose(), ax=ax)
ax.set_xlabel('PEPP predicted')
ax.set_ylabel('PEPP')


def modelPerfPerFile(data, legend=True, ax=None):
    "Plot performance with color code based on filenames"
    if ax is None:
        fig, ax = plt.subplots(figsize=(15,10))
        
    markerseq = itertools.cycle(('+', 'v', 'x', 's', 'D', 'X'))
    for name, group in data.groupby('rawDataFile'):
        plt.plot(group['PEPP_predict'], group['PHOTON-ENERGY-PER-PULSE'], alpha=1.0, marker=next(markerseq), linestyle='', label=name)
    ax.set_xlabel('PEPP predicted')
    ax.set_ylabel('PEPP')
    if legend:
        ax.legend()


tst_df = test
tst_df['PEPP_predict'] = y_predict
modelPerfPerFile(tst_df, legend=False)

# +
tst_df = test
tst_df['PEPP_predict'] = y_predict

thresh = 40.0
tst1_df = tst_df[ (tst_df['PHOTON-ENERGY-PER-PULSE'] - tst_df['PEPP_predict']).abs() > thresh]
tst2_df = tst_df[ (tst_df['PHOTON-ENERGY-PER-PULSE'] - tst_df['PEPP_predict']).abs() <= thresh]

fig, ax = plt.subplots(figsize=(15,10))
#ax.plot(y_predict, test['PHOTON-ENERGY-PER-PULSE'], marker='o', linestyle='', alpha=0.01)
ax.plot(tst2_df['PEPP_predict'], tst2_df['PHOTON-ENERGY-PER-PULSE'], marker='o', linestyle='',
        alpha=0.05, label='')
modelPerfPerFile(tst1_df, legend=True, ax=ax)
# -

tst2_df.shape

# Prepare a cut of the data containing interesting mispredicted areas
#
# I pick a region with a substantial number of errors

cutfilter = (y_predict >= 110) & (y_predict <= 115)
tst_df = test[cutfilter]
tst_df['PEPP_predict'] = y_predict[cutfilter]
modelPerfPerFile(tst_df)

sns.pairplot(tst_df[['CALCT', 'CALCS', 'SPECTRUM_CENTER','XeMultVoltag','PHOTON-ENERGY-PER-PULSE','PEPP_predict']])

# Let's look at an area where we have good predictions

cutfilter = (y_predict >= 130) & (y_predict <= 200)
tst_df = test[cutfilter]
tst_df['PEPP_predict'] = y_predict[cutfilter]
modelPerfPerFile(tst_df)

sns.pairplot(tst_df[['CALCT', 'CALCS', 'SPECTRUM_CENTER','XeMultVoltag','PHOTON-ENERGY-PER-PULSE','PEPP_predict']])

cutfilter = (y_predict >= 220) & (y_predict <= 330)
tst_df = test[cutfilter]
tst_df['PEPP_predict'] = y_predict[cutfilter]
modelPerfPerFile(tst_df)

# ### outlier investigation after answer by Jochem/Pavle

data[data['PHOTON-ENERGY-PER-PULSE'] == 0.0]

dftmp = pd.read_csv('cleaned/dp30-nomeans.csv', sep=";")
dftmp.head()

dftmp = pd.read_csv('cleaned/dp30-nomeans.csv', sep=";")
dftmp = dftmp[['SARFE10-PBIG050-EVR0:CALCT.value', 
                    'SARFE10-PBIG050-EVR0:CALCS.value',
                    'SARFE10-PSSS059:SPECTRUM_CENTER.value',
                    'SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.value']]
dftmp.columns = ['CALCT','CALCS','SPECTRUM_CENTER','PHOTON-ENERGY-PER-PULSE']

dftmp.info()

dftmp[~dftmp['PHOTON-ENERGY-PER-PULSE'].isna()].plot(y='PHOTON-ENERGY-PER-PULSE')

dftmp2 = dftmp[~dftmp['PHOTON-ENERGY-PER-PULSE'].isna()]

dftmp2[dftmp2['PHOTON-ENERGY-PER-PULSE'] == 0.0]

# use sorted file list for reproducibility
flist = glob.glob(os.path.join(directory,'dp*-nomeans.csv'))
flist.sort()

dffull=pd.DataFrame()
for fname in flist:
    try:
        dftmp = pd.read_csv(fname, sep=";")
    except:
        print(f"Failure CSV parsing of {fname}")
        continue
    dftmp = dftmp[['SARFE10-PBIG050-EVR0:CALCT.value', 
                    'SARFE10-PBIG050-EVR0:CALCS.value',
                    'SARFE10-PSSS059:SPECTRUM_CENTER.value',
                    'SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.value']]
    dftmp.columns = ['CALCT','CALCS','SPECTRUM_CENTER','PHOTON-ENERGY-PER-PULSE']
    dftmp['filename'] = re.sub('.*\/','', fname)
    dffull = pd.concat([dffull, dftmp[dftmp['PHOTON-ENERGY-PER-PULSE'] == 0.0]])


dffull

# ## Trying to reproduce best hyperscan run
# Based on offline batch run from the Hyperparameter scan chapter.
# Scan is still running. Best result as of Mon Jan 20 10:24h
#
# Based on logfile this run took 7400s

# %%time
model_scanbest = build_ff_mdl_small(in_dim = x_train.shape[1], out_dim = y_train.shape[1],
                                    l1 = 4*8, l2 = 4*6, l3 = 4*4, l4= 4*4,
                                    opt = 'adam', loss = 'mse', act = 'tanh', l2reg = 0.00, gn=0.001)
#mc = keras.callbacks.ModelCheckpoint('best_model_1.h5', monitor='val_loss', mode='min', save_best_only=True)
es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0 = model_scanbest.fit(x=x_train, y=y_train, 
                     validation_data=(x_validate,y_validate),
                     batch_size=50, shuffle='true',epochs=10,  #2000, 
                     verbose='false', callbacks=[plot_losses,es])

model_scanbest.summary()

# I ran this model offline as well and saved it

model_scanbest = keras.models.load_model('model-scanbest.h5')

print_model_err(model_scanbest, x_test, y_test)

# %%time
#plotModelPerf2dens(model_0, 'Model 0', x_test[0:1000,:], y_test[0:1000,:], test[['PHOTON-ENERGY-PER-PULSE']].columns, transformer_y, '.')
plotModelPerf2dens(model_scanbest, 'Model from best scan', x_test, y_test,
                   test[var_dep].columns, transformer_y, '.')

# The outliers show some systematic patterns. Actually they seem to be primarily confined to a mostly discrete number of predicted energies in the surrogate model. **Could there be some potential systematic errors in the measurements?**

# %%time
plotModelPerf2(model_scanbest, 'Model from best scan', x_test, y_test, test[var_dep].columns, transformer_y, '.')

# %%time
#plotModelPerf2dens(model_0, 'Model 0', x_test[0:1000,:], y_test[0:1000,:], test[['PHOTON-ENERGY-PER-PULSE']].columns, transformer_y, '.')
plotModelPerf2(model_scanbest, 'Model from best scan', x_train, y_train, test[var_dep].columns, transformer_y, '.')

# ## Try out DNN with dropout
# Since the model0 without any regularization shows just very minor overfitting within the tests, and also the hyperparameter scan points as of now to low gn being best, dropout probably makes no sense at all. Still, in order to get a bit more acquainted with the features, I ran a few tests.
#
# Dropout in all cases made the fits worse, but I think there may be some systematic error or general problem with the dropout network. I would like to understand the following.
#
# - **Why is the validation set loss always lower than the training set loss?**
# - **In the Model performance plots, why are all predicted values in a constrained interval band compared to the "ground truth" values?**

# %%time
model_Dout_1 = build_ff_mdl_smallDropOut(in_dim  = x_train.shape[1], 
                             out_dim = y_train.shape[1], dout=0.2)
#mc = keras.callbacks.ModelCheckpoint('best_model_1.h5', monitor='val_loss', mode='min', save_best_only=True)
es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0_Dout_1 = model_Dout_1.fit(x=x_train, y=y_train, 
                     validation_data=(x_validate,y_validate),
                     batch_size=250, shuffle='true',epochs=500,  #2000, 
                     verbose='false', callbacks=[plot_losses,es])

print_model_err(model_Dout_1, x_test, y_test)

# %%time
#plotModelPerf2dens(model_0, 'Model 0', x_test[0:1000,:], y_test[0:1000,:], test[['PHOTON-ENERGY-PER-PULSE']].columns, transformer_y, '.')
plotModelPerf2dens(model_Dout_1, 'Model Dout_1', x_test, y_test, test[['PHOTON-ENERGY-PER-PULSE']].columns, transformer_y, '.')

# %%time
#plotModelPerf2dens(model_0, 'Model 0', x_test[0:1000,:], y_test[0:1000,:], test[['PHOTON-ENERGY-PER-PULSE']].columns, transformer_y, '.')
plotModelPerf2(model_Dout_1, 'Model Dout_1', x_train, y_train, test[['PHOTON-ENERGY-PER-PULSE']].columns, transformer_y, '.')

# %%time
model_Dout_2 = build_ff_mdl_smallDropOut(in_dim  = x_train.shape[1], 
                             out_dim = y_train.shape[1], dout=0.5)
#mc = keras.callbacks.ModelCheckpoint('best_model_1.h5', monitor='val_loss', mode='min', save_best_only=True)
es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0_Dout_2 = model_Dout_2.fit(x=x_train, y=y_train, 
                     validation_data=(x_validate,y_validate),
                     batch_size=250, shuffle='true',epochs=500,  #2000, 
                     verbose='false', callbacks=[plot_losses,es])

print_model_err(model_Dout_2, x_test, y_test)

# %%time
#plotModelPerf2dens(model_0, 'Model 0', x_test[0:1000,:], y_test[0:1000,:], test[['PHOTON-ENERGY-PER-PULSE']].columns, transformer_y, '.')
plotModelPerf2(model_Dout_2, 'Model Dout_2', x_test, y_test, test[['PHOTON-ENERGY-PER-PULSE']].columns, transformer_y, '.')

# %%time
model_Dout_3 = build_ff_mdl_smallDropOut(in_dim  = x_train.shape[1], 
                             out_dim = y_train.shape[1], dout=0.1)
#mc = keras.callbacks.ModelCheckpoint('best_model_1.h5', monitor='val_loss', mode='min', save_best_only=True)
es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0_Dout_3 = model_Dout_3.fit(x=x_train, y=y_train, 
                     validation_data=(x_validate,y_validate),
                     batch_size=250, shuffle='true',epochs=500,  #2000, 
                     verbose='false', callbacks=[plot_losses,es])

# Model with higher number of neurons and dropout

model_dropout_m4 = keras.models.load_model('model-dropout_m4.h5')
model_dropout_m4.summary()

model_dropout_m4_hist = pd.read_csv("./model-dropout_m4-hist.csv")

model_dropout_m4_hist.plot(y=['loss', 'val_loss'])

print_model_err(model_dropout_m4, x_test, y_test)

# # Hyperparameter scans (data not corrected for zeroes)

# ## Test: Make a hyperparameter scan A

# +
from keras.activations import relu, elu

#params = {
#    'mult_neuron': [1, 2, 4],
#    'activation': ['relu', 'elu', 'tanh'],
#    'batch_size': [10, 25, 50, 100]
#}

params = {
    'mult_neuron': [1],
    'activation': ['tanh', 'relu'],
    'batch_size': [125, 250]
}
# -

scanObj = ta.Scan(x=x_train, y=y_train, x_val=x_validate, y_val=y_validate, 
            params=params, model=modelA, experiment_no='1', dataset_name='swissfel-modelA');

modelAEval = ta.Evaluate(scanObj)

modelAEval.data.sort_values(by=['mean_squared_error'])

# ## Offline batch parameter scan
# In order to get more work done, I ran this scan offline using a batch script containing the following:
#
# Model: build_ff_mdl_smallA
#
# ```
# params = {
#     'mult_neuron': [1, 2, 4],
#     'activation': ['relu', 'elu', 'tanh'],
#     'batch_size': [10, 25, 50, 100],
#     'noise': [0.1, 0.01, 0.001]
# }
# ```

scan_df1_perf = pd.read_csv("./swissfel-modelA-df_1.csv")

scan_df1_perf.sort_values('val_loss')

# ## TODO ModelB

params = {
    'mult_neuron': [4,5],
    'activation': ['relu','tanh'],
    'batch_size': [5, 10]}

scanObjB = ta.Scan(x=x_train, y=y_train, x_val=x_validate, y_val=y_validate, 
                   params=params, model=modelB, experiment_no='2', dataset_name='swissfel-modelB')

scanObjB

scanObjBEval = ta.Evaluate(scanObjB)
scanObjBEval.data.sort_values(by=['mean_squared_error'])

# +
params = {
    'mult_neuron': 5,
    'activation': 'tanh',
    'batch_size': 10
}

es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=1000)

modelB = build_ff_mdl_smallB(params, in_dim = x_train.shape[1],out_dim = y_train.shape[1],
                             l1 = params['mult_neuron']*8, l2 = params['mult_neuron']*6,
                             l3 = params['mult_neuron']*4, l4 = params['mult_neuron']*4, l2reg=0.0)
    
# make sure history object is returned by model.fit()
out = modelB.fit(x=x_train,
                y=y_train,
                validation_data=[x_validate, y_validate],
                epochs=2000, shuffle='true',
                batch_size=params['batch_size'],
                verbose='false', callbacks=[plot_losses,es])
# -

plotModelPerf2(modelB, 'x5, tanh bs=10', x_train, y_train, data[['SPECTRUM_CENTER']].columns, transformer_y, '.')

plotModelPerf2(modelB, 'x5, tanh bs=10', x_test, y_test, data[['SPECTRUM_CENTER']].columns, transformer_y, '.')

# ## TODO: Model C: scan regulatisation and noise

params = {
    'mult_neuron': [5,6],
    'activation': ['relu'],
    'batch_size': [5, 10],
    'l2reg': [0.0, 0.0001],
    'noise': [0.1, 0.01, 0.001]}

scanObjC = ta.Scan(x=x_train, y=y_train, x_val=x_validate, y_val=y_validate, 
            params=params, model=modelC, experiment_no='1', dataset_name='swissfel-modelC')



def makeData(directory,fn):
    try:
        dp = pd.read_csv(directory+fn, sep=";") 
        dpc1 = dp[["SARFE10-PBIG050-EVR0:CALCT.value","SARFE10-PBIG050-EVR0:CALCS.value"]].dropna()
        dpc2 = dp[["SARFE10-PSSS059:SPECTRUM_CENTER.value","SARFE10-PBIG050-EVR0:CALCI.value"]].dropna()
        dpc3 = dp[["SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-DS.value","SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-US.value",
                       "SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.value"]].dropna()
        dpc1.columns = ['CALCT','CALCS']
        dpc2.columns = ['SPECTR_CENT','CALCI']
        dpc3.columns = ['PHE-PP-DS','PHE-PP-US','PHE-PP-AVG']
        return dpc1,dpc2,dpc3,dp,1
    except:
        print ("Can not read " + fn)
        return 0,0,0,0,0


dp = pd.read_csv('/shared-scratch/adelmann/SwissFEL-Gas-1/cleaned/dp00-nomeans.csv', sep=";") 

dp[['SARFE10-PBIG050-EVR0:CALCT.value','SARFE10-PBIG050-EVR0:CALCS.value']].plot() 

dp.columns

x=dp[['SARFE10-PBIG050-EVR0:CALCT.value',
   'SARFE10-PSSS059:SPECTRUM_CENTER.value','SARFE10-PBIG050-EVR0:CALCS.value']]

y=dp['SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.value']

dp['SARFE10-PSSS059:SPECTRUM_CENTER.value'].dropna().plot()

y.dropna().plot()





def readDataSetInterpolated(directory, excelFn, doInterpolate=True, dropBadPulses=True, verbose=False):
    first = True
    data = []
    for filename in sorted(os.listdir(directory)):
        if filename.endswith(".csv"):
            fntmp = re.sub(r'.*dp', '', filename)
            expNumber = re.sub(r'-nomeans.csv', '', fntmp)
            file_excel = pd.read_excel(excelFn)
            multVoltag = file_excel.iloc[int(expNumber)]['XeMultVoltag'] 
            try:
                dp  = pd.read_csv(directory+filename, sep=";") 
            except:
                print ("Can not read " + directory + filename)
                continue
                
#            dp = dp.dropna();

            dp['XeMultVoltag'] = multVoltag
            dp['rawDataFile']  = filename
            
            if first:
                data = dp
                first = False
            else:
                data = data.append(dp,ignore_index=True)

            if verbose:
                print("Datapoint", expNumber, "gave", len(dp), "values")
    data.reset_index(inplace=True)
    data.dropna()
    return data


directory = "/psi/home/adelmann/data/ml-gasmon/cleaned/"
xlsxFn    = '/psi/home/adelmann/data/ml-gasmon/XeX77.xlsx'
data      = readDataSetInterpolated(directory,xlsxFn)

for col in data.columns: 
    print(col) 

ax=data[['SARFE10-PBIG050-EVR0:CALCI.value','SARFE10-PBIG050-EVR0:CALCS.value',
     'SARFE10-PBIG050-EVR0:CALCT.value']].plot()
ax.legend(loc='center left', bbox_to_anchor=(0.0, -0.3))

ax=data[['SARFE10-PBPG050:ENERGY.value','SARFE10-PSSS059:SPECTRUM_CENTER.value','XeMultVoltag']].plot()
ax.legend(loc='center left', bbox_to_anchor=(0.0, -0.3))

ax=data[['SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.value','SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-DS.value',
     'SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-US.value']].plot()
ax.legend(loc='center left', bbox_to_anchor=(0.0, -0.3))

data['SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-DS.value'].plot()

data['SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-US.value'].plot()

data['SARFE10-PBPG050:ENERGY.value'].plot()

# # DNN runs with data set cleaned for zero energy measurements

# Make sure that the zero measurements are no longer in the data set

data[ data['PHOTON-ENERGY-PER-PULSE'] == 0.0]

# ## without any regularization

# %%time
model_0_noreg_c2 = build_ff_mdl_small(in_dim  = x_train.shape[1], out_dim = y_train.shape[1],
                                  l2reg=0.0, gn=0.0)
#mc = keras.callbacks.ModelCheckpoint('best_model_1.h5', monitor='val_loss', mode='min', save_best_only=True)
es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0_noreg_c2 = model_0_noreg_c2.fit(x=x_train, y=y_train, 
                     validation_data=(x_validate,y_validate),
                     batch_size=250, shuffle='true',epochs=2000, 
                     verbose='false', callbacks=[plot_losses,es])

model_0_noreg_c2.save('model_0_noreg_c2.h5')

print_model_err(model_0_noreg_c2, x_test, y_test)

# %%time
plotModelPerf2(model_0_noreg_c2, 'Model0 No Regularization', x_test, y_test, test[var_dep].columns, transformer_y, '.')

# %%time
model_0_noreg_m4_c2 = build_ff_mdl_small(in_dim  = x_train.shape[1], out_dim = y_train.shape[1],
                                      l1 = 4*8, l2 = 4*6, l3 = 4*4, l4= 4*4,
                                  l2reg=0.0, gn=0.0)
#mc = keras.callbacks.ModelCheckpoint('best_model_1.h5', monitor='val_loss', mode='min', save_best_only=True)
es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0_noreg_m4_c2 = model_0_noreg_m4_c2.fit(x=x_train, y=y_train, 
                     validation_data=(x_validate,y_validate),
                     batch_size=250, shuffle='true',epochs=2000, 
                     verbose='false', callbacks=[plot_losses,es])

model_0_noreg_m4_c2.save('model_0_noreg_m4_c2.h5')

pd.DataFrame(hist_0_noreg_m4_c2.history).to_csv('model_0_noreg_m4_c2-hist.csv')

print_model_err(model_0_noreg_m4_c2, x_test, y_test)

model_0_noreg_m4_c2 = keras.models.load_model('model_0_noreg_m4_c2.h5')

# %%time
plotModelPerf2(model_0_noreg_m4_c2, 'Model0 m4 No Regularization', x_test, y_test, test[var_dep].columns, transformer_y, '.')

# %%time
plotModelPerf2dens(model_0_noreg_m4_c2, 'Model0 m4 No Regularization', x_test, y_test, test[var_dep].columns, transformer_y, '.')

# # SVM to see what a linear model can do

clf = svm.SVR()
clf.fit(x_train, y_train)
y_pred_svm=clf.predict(x_test).ravel()
print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred_svm))
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred_svm))
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred_svm)))
y_test.shape

y_pred_svm.shape

y_pred_svm.reshape(-1,1).shape

fig,ax = plt.subplots(figsize=(12,8))
ax.plot(transformer_y.inverse_transform(y_pred_svm.reshape(-1,1)),
        transformer_y.inverse_transform(y_test), linestyle='', marker='o', alpha=0.05)
ax.set_xlabel('PEPP from model')
ax.set_ylabel('PEPP')

# # Test for leaving out some files from the training set
# By examining the source data, I get the impression that each file only contains a very small subspace of the total parameter space. So, it somehow is more that we have 58 measurement points with some confidence intervals, instead of 15000 measurements.


# +
fchoice = "33,34"
flist = [f'rawDataFile != "dp{fnum}-nomeans.csv"' for fnum in fchoice.split(",")]
trainB, validateB, testB = np.split(data.query(" & ".join(flist)) \
                                        .sample(frac=1), [int(.6*len(data)), int(.8*len(data))])

x_trainB    = trainB[var_indep]
y_trainB    = trainB[var_dep]
x_validateB = validateB[var_indep]
y_validateB = validateB[var_dep]
x_testB     = testB[var_indep]
y_testB     = testB[var_dep]

# +
flist = [f'rawDataFile == "dp{fnum}-nomeans.csv"' for fnum in fchoice.split(",")]
data_ignf = data.query(" | ".join(flist))
x_testB_ignf = np.concatenate((x_testB, data_ignf[var_indep].to_numpy()))
y_testB_ignf = np.concatenate((y_testB, data_ignf[var_dep].to_numpy()))

x_testB_ignf     = transformer_x.transform(x_testB_ignf)
y_testB_ignf     = transformer_y.transform(y_testB_ignf)

# +
x_trainB    = transformer_x.transform(x_trainB)
x_validateB = transformer_x.transform(x_validateB)
x_testB     = transformer_x.transform(x_testB)

y_trainB    = transformer_y.transform(y_trainB)
y_validateB = transformer_y.transform(y_validateB)
y_testB     = transformer_y.transform(y_testB)
# -
x_trainB.shape, x_validateB.shape

# %%time
model_0_noreg_m4_B = build_ff_mdl_small(in_dim  = x_trainB.shape[1], out_dim = y_trainB.shape[1],
                                      l1 = 4*8, l2 = 4*6, l3 = 4*4, l4= 4*4,
                                  l2reg=0.0, gn=0.0)
es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0_noreg_m4_B = model_0_noreg_m4_B.fit(x=x_trainB, y=y_trainB, 
                     validation_data=(x_validateB,y_validateB),
                     batch_size=250, shuffle='true',epochs=300, 
                     verbose='false', callbacks=[plot_losses,es])

# %%time
plotModelPerf2(model_0_noreg_m4_B, 'Model0 m4 No Regularization', x_testB, y_testB, testB[var_dep].columns, transformer_y, '.')

# Now I visualize the performance including the samples from the unseen files

# %%time
plotModelPerf2(model_0_noreg_m4_B, 'Model0 m4 No Regularization', x_testB_ignf, y_testB_ignf, testB[var_dep].columns, transformer_y, '.')


# # new data splitting in respect to separate files

# - training, validate, and test data must be split according to source files, since each file contains points measured at intentionally at constant conditions
# - should we rather use a mean value per measurement instead of the single points within each file?

# data.drop('index', axis=1).groupby('rawDataFile').agg(['mean','std'])
data_mean = data.drop('index', axis=1).groupby('rawDataFile').mean()

# Let's have a look at the mean/stdev of the files with the fewest points

pd.concat([data[['CALCT','rawDataFile']].groupby('rawDataFile').count() \
                             .rename(columns={'CALCT': 'count'}),
          data.drop('index', axis=1).groupby('rawDataFile').agg(['mean','std'])],
         axis=1) \
         .query('count < 50').sort_values('count')

data.query('rawDataFile == "dp75-nomeans.csv"')

sns.pairplot(data_mean)

# ## tests using mean values per file

fnames = data.rawDataFile.unique()
ftrain, fvalidate, ftest = np.split(pd.Series(fnames).sample(frac=1.0),
                                    [int(x) for x in len(fnames)*np.array([0.9,1])])
[len(x) for x in [ftrain, fvalidate, ftest]]

var_indep = ['SPECTRUM_CENTER','XeMultVoltag','CALCT','CALCS']
var_dep = ['PHOTON-ENERGY-PER-PULSE']

# I'm still defining the scalers on the base of the full data
transformer_x = MinMaxScaler(feature_range=(-1, 1)).fit(data[var_indep].values)
transformer_y = MinMaxScaler(feature_range=(-1, 1)).fit(data[var_dep].values)

# +
x_train_mean = data_mean.loc[ftrain, var_indep]
y_train_mean = data_mean.loc[ftrain, var_dep]

x_valid_mean = data_mean.loc[fvalidate, var_indep]
y_valid_mean = data_mean.loc[fvalidate, var_dep]

x_test_mean = data_mean.loc[ftest, var_indep]
y_test_mean = data_mean.loc[ftest, var_dep]

x_train_mean = transformer_x.transform(x_train_mean)
x_valid_mean = transformer_x.transform(x_valid_mean)
#x_test_mean     = transformer_x.transform(x_test_mean)

y_train_mean = transformer_y.transform(y_train_mean)
y_valid_mean = transformer_y.transform(y_valid_mean)
#y_test_mean     = transformer_y.transform(y_valid_mean)
# -
# ### DNN toy run
# Just for the fun of it, I try training the previous kind of NNs, expecting a lot of overfitting due to the limited data in relation to the degrees of freedom.

model_0_mean = build_ff_mdl_small(in_dim  = x_train_mean.shape[1], 
                             out_dim = y_train_mean.shape[1],
                                 l2reg=0.0, gn=0.0)

model_0_mean.summary()

es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0_mean = model_0_mean.fit(x=x_train_mean, y=y_train_mean, 
                     validation_data=(x_valid_mean,y_valid_mean),
                     batch_size=60, shuffle='true',epochs=500,  #2000, 
                     verbose='false', callbacks=[plot_losses,es])

plotModelPerf2(model_0_mean, 'Test with set of mean values', x_train_mean, y_train_mean,
               data_mean[var_dep].columns, transformer_y, '.',alpha=1.0)
plotModelPerf2(model_0_mean, 'Test with set of mean values', x_valid_mean, y_valid_mean,
               data_mean[var_dep].columns, transformer_y, '.',alpha=1.0)

model_0_mean_l4 = build_ff_mdl_small(in_dim  = x_train_mean.shape[1], 
                             out_dim = y_train_mean.shape[1],
                                  l1 = 4*8, l2 = 4*6, l3 = 4*4, l4= 4*4,
                                 l2reg=0.0, gn=0.0)

model_0_mean_l4.summary()

es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0_mean_l4 = model_0_mean_l4.fit(x=x_train_mean, y=y_train_mean, 
                     validation_data=(x_valid_mean,y_valid_mean),
                     batch_size=60, shuffle='true',epochs=500,  #2000, 
                     verbose='false', callbacks=[plot_losses,es])

plotModelPerf2(model_0_mean_l4, 'Test with set of mean values', x_train_mean, y_train_mean,
               data_mean[var_dep].columns, transformer_y, '.',alpha=1.0)
plotModelPerf2(model_0_mean_l4, 'Test with set of mean values', x_valid_mean, y_valid_mean,
               data_mean[var_dep].columns, transformer_y, '.',alpha=1.0)

# ### l1 reg

model_10_mean = build_dnn_l1_small(in_dim  = x_train_mean.shape[1], 
                             out_dim = y_train_mean.shape[1],
                                 l1reg=0.0001)

model_10_mean.summary()

es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_10_mean = model_10_mean.fit(x=x_train_mean, y=y_train_mean, 
                     validation_data=(x_valid_mean,y_valid_mean),
                     batch_size=60, shuffle='true',epochs=500,  #2000, 
                     verbose='false', callbacks=[plot_losses,es])

# I record here the points that went into the validation/test sets
fvalidate, ftest

plotModelPerf2(model_10_mean, 'Test with set of mean values', x_train_mean, y_train_mean,
               data_mean[var_dep].columns, transformer_y, '.',alpha=1.0)
plotModelPerf2(model_10_mean, 'Test with set of mean values', x_valid_mean, y_valid_mean,
               data_mean[var_dep].columns, transformer_y, '.',alpha=1.0)

# ## using full data set, but validation split off according to files

fnames = data.rawDataFile.unique()
ftrain, fvalidate, ftest = np.split(pd.Series(fnames).sample(frac=1.0),
                                    [int(x) for x in len(fnames)*np.array([0.9,1])])
[len(x) for x in [ftrain, fvalidate, ftest]]

var_indep = ['SPECTRUM_CENTER','XeMultVoltag','CALCT','CALCS']
var_dep = ['PHOTON-ENERGY-PER-PULSE']

transformer_x = MinMaxScaler(feature_range=(-1, 1)).fit(data[var_indep].values)
transformer_y = MinMaxScaler(feature_range=(-1, 1)).fit(data[var_dep].values)

# +
x_train_fileb = transformer_x.transform(data.query('rawDataFile in @ftrain')[var_indep])
y_train_fileb = transformer_y.transform(data.query('rawDataFile in @ftrain')[var_dep])

x_valid_fileb = transformer_x.transform(data.query('rawDataFile in @fvalidate')[var_indep])
y_valid_fileb = transformer_y.transform(data.query('rawDataFile in @fvalidate')[var_dep])

#x_test_fileb = transformer_x.transform(data.query('rawDataFile in @ftest')[var_indep])
#y_test_fileb = transformer_y.transform(data.query('rawDataFile in @ftest')[var_dep])

# -

# ### original initial DNN

# %%time
model_0_fileb = build_ff_mdl_small(in_dim  = x_train_fileb.shape[1], 
                             out_dim = y_train_fileb.shape[1])
#mc = keras.callbacks.ModelCheckpoint('best_model_1.h5', monitor='val_loss', mode='min', save_best_only=True)
es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_0_fileb = model_0_fileb.fit(x=x_train_fileb, y=y_train_fileb, 
                     validation_data=(x_valid_fileb,y_valid_fileb),
                     batch_size=250, shuffle='true',epochs=100,  #2000, 
                     verbose='false', callbacks=[plot_losses,es])

plotModelPerf2(model_0_fileb, 'Training data fit', x_train_fileb, y_train_fileb,
               data[var_dep].columns, transformer_y, '.',alpha=0.05)
plotModelPerf2(model_0_fileb, 'Validation data fit', x_valid_fileb, y_valid_fileb,
               data[var_dep].columns, transformer_y, '.',alpha=0.05)



# ### l1 reg

model_11_fileb = build_dnn_l1_small(in_dim  = x_train_fileb.shape[1], 
                             out_dim = y_train_fileb.shape[1],
                                 l1reg=0.0001)

es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)
hist_11_fileb = model_11_fileb.fit(x=x_train_fileb, y=y_train_fileb, 
                     validation_data=(x_valid_fileb,y_valid_fileb),
                     batch_size=250, shuffle='true',epochs=500,  #2000, 
                     verbose='false', callbacks=[plot_losses,es])

plotModelPerf2(model_11_fileb, 'Test with set of mean values', x_train_fileb, y_train_fileb,
               data_mean[var_dep].columns, transformer_y, '.',alpha=0.05)
plotModelPerf2(model_11_fileb, 'Test with set of mean values', x_valid_fileb, y_valid_fileb,
               data_mean[var_dep].columns, transformer_y, '.',alpha=0.05)

plotModelPerf2(model_0_mean_l4, 'Test with set of mean values', x_train_fileb, y_train_fileb,
               data_mean[var_dep].columns, transformer_y, '.',alpha=0.05)
plotModelPerf2(model_0_mean_l4, 'Test with set of mean values', x_valid_fileb, y_valid_fileb,
               data_mean[var_dep].columns, transformer_y, '.',alpha=0.05)

# # physics based model

# data.drop('index', axis=1).groupby('rawDataFile').agg(['mean','std'])
data_mean = data.drop('index', axis=1).groupby('rawDataFile').mean()

data_mean.reset_index().head(3)

# ## Xe data on cross section, charge vs photon energy

xedf = pd.read_csv('./backgroundref/Xe.DAT', delim_whitespace=True, comment='#',
                  skiprows=[1]) \
         .rename(columns={"Xe": "cross-section", "Xe.gamma": "charge"})

xedf.head()

# +
# prepare dataframe containing photon energies from data set
tmpdf = pd.DataFrame()
tmpdf['Energy'] = data_mean.SPECTRUM_CENTER
tmpdf['cross-section'] = np.NaN
#tmpdf['rawDataFile'] = 'placeholder'
tmpdf['charge'] = np.NaN
tmpdf.set_index('Energy', inplace=True)

# Join with Xe data set for interpolation
tmpdf = pd.concat([tmpdf, xedf.reset_index(drop=True).set_index('Energy')])
tmpdf.head()
# -

tmpdf = tmpdf.sort_index() \
     .interpolate(method='index')

# test whether interpolation has worked
tmpdf.loc[(tmpdf.index > 6100) & (tmpdf.index < 6105)]

fig,ax = plt.subplots(figsize=(12,6))
tmpdf['cross-section'].plot(marker='.', ax=ax)
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_ylabel('cross section')

# +
physdf = pd.concat([data_mean.reset_index().set_index('SPECTRUM_CENTER'), tmpdf], axis=1, join='inner') \
        .reset_index() \
        .rename(columns = {'index': 'SPECTRUM_CENTER'})

physdf.head()
# -

# The Electromultiplier signals like CALCS should be proportional to the number of photoions hitting the detector (multiplier was operated within the linear voltage range). We do not know the number of photons hitting the EM, but we know that it must be proportional to the number of photons $N_{ph}$ in the beam, which we obtain from the ion current detector.
#
#  \begin{aligned} N_{ph} = \frac{\Phi}{\hbar \omega} = \frac{k}{e} \dot{} \frac{I_{\text {ion}} T}{\gamma \sigma z p} \end{aligned}
#  
# In the present data set we already get the avg. photon energy per pulse, so we can get the average photons per pulse directly by
#  
#  \begin{aligned} N_{ph} = \frac{{\text {PE}}_{\text {avg}}}{\hbar \omega} \end{aligned}

physdf['Nsigma_avg'] = physdf['PHOTON-ENERGY-PER-PULSE'] * physdf['cross-section'] / physdf['SPECTRUM_CENTER']
physdf['Nphot_avg'] = physdf['PHOTON-ENERGY-PER-PULSE'] / physdf['SPECTRUM_CENTER']
physdf['CALCSUM'] = physdf.CALCS + physdf.CALCT


physdf.describe()

fig,ax = plt.subplots(2,1,figsize=(12,12))
physdf.plot(kind='scatter', x='CALCT', y='Nsigma_avg', ax=ax[0], alpha=0.5)
physdf.plot(kind='scatter', x='CALCT', y='Nphot_avg', ax=ax[1], alpha=0.5)

physdf.XeMultVoltag.plot(linestyle='', marker='o')

physdf.XeMultVoltag.sort_values().reset_index(drop=True).plot(linestyle='', marker='o')

# Let's look at a sorted plot of EM Voltage, and how the photonE correlates
fig,axes=plt.subplots(1,2, figsize=(12,6))
physdf.sort_values(by='XeMultVoltag').reset_index(drop=True) \
      .plot(y='XeMultVoltag',linestyle='', marker='o', ax=axes[0])
physdf.sort_values(by='XeMultVoltag').reset_index(drop=True) \
      .plot(y='SPECTRUM_CENTER',linestyle='', marker='o', ax=axes[1])

physdf.plot(x='SPECTRUM_CENTER', y='XeMultVoltag', linestyle='', marker='o')

# +
tmpdf = physdf.query('1190 < XeMultVoltag < 1230')
markerseq = itertools.cycle(('+', 'v', 'x', 's', 'D', 'X'))
agrid = sns.lmplot(x='Nphot_avg', y='CALCS', hue="rawDataFile",
           markers=[next(markerseq) for _ in tmpdf.XeMultVoltag.unique()],
              data=tmpdf,
              fit_reg=False, size=4, aspect=3)

for row in range(0, tmpdf.shape[0]):
    #agrid.axes[0][0].text(tmpdf.Nphot_avg.iloc[0]+0.002, tmpdf.CALCS.iloc[0], "test")
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.0005, tmpdf.CALCS.iloc[row],
                  np.round(tmpdf.XeMultVoltag.iloc[row],1))    
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.0005, tmpdf.CALCS.iloc[row]-440,
                  np.round(tmpdf.SPECTRUM_CENTER.iloc[row],1))    


# +
tmpdf = physdf.query('850 < XeMultVoltag < 950')
markerseq = itertools.cycle(('+', 'v', 'x', 's', 'D', 'X'))
agrid = sns.lmplot(x='Nphot_avg', y='CALCS', hue="rawDataFile",
           markers=[next(markerseq) for _ in tmpdf.XeMultVoltag.unique()],
              data=tmpdf,
              fit_reg=False, size=4, aspect=3)

for row in range(0, tmpdf.shape[0]):
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.0005, tmpdf.CALCS.iloc[row],
                  np.round(tmpdf.XeMultVoltag.iloc[row],1))    
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.0005, tmpdf.CALCS.iloc[row]-50,
                  np.round(tmpdf.SPECTRUM_CENTER.iloc[row],1))    


# +
tmpdf = physdf.query('1400 < XeMultVoltag < 1530')
markerseq = itertools.cycle(('+', 'v', 'x', 's', 'D', 'X'))
agrid = sns.lmplot(x='Nphot_avg', y='CALCS', hue="rawDataFile",
           markers=[next(markerseq) for _ in tmpdf.Nphot_avg],
              data=tmpdf,
              fit_reg=False, size=4, aspect=3)

for row in range(0, tmpdf.shape[0]):
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.002, tmpdf.CALCS.iloc[row],
                  np.round(tmpdf.XeMultVoltag.iloc[row],1))    
    agrid.ax.text(tmpdf.Nphot_avg.iloc[row]+0.002, tmpdf.CALCS.iloc[row]-1100,
                  np.round(tmpdf.SPECTRUM_CENTER.iloc[row],1))    

# -

import scipy


# Try to define the gain based function
# \begin{aligned} {\text {CALCS}} = N_{phot} \cdot k_{gain} \cdot Voltage_{EM}^{k_{instrument}} \end{aligned}

# x is composed of 2 arrays: [ nphot, EMvoltage]
def calcs_fn(x, kcoeff, kpow):
    return x[0] * kcoeff * np.power(x[1], kpow) 


[ tmpdf.Nphot_avg.iloc[0:2], tmpdf.XeMultVoltag.iloc[0:2], tmpdf.CALCS.iloc[0:2] ]

calcs_fn([ tmpdf.Nphot_avg.iloc[0:2], tmpdf.XeMultVoltag.iloc[0:2] ],                                    
         -40, 1.0)

calcs_fn([ tmpdf.Nphot_avg.iloc[0], tmpdf.XeMultVoltag.iloc[0] ],                                    
         -8e-15, 5.87)

popt, pcov = scipy.optimize.curve_fit(calcs_fn,
                                      [ tmpdf.Nphot_avg, tmpdf.XeMultVoltag ],
                                      tmpdf.CALCS,
                                      [-40.0, 1.0],
                                      maxfev=10000
                                     )

popt

fig, ax = plt.subplots()
ax.plot(tmpdf.Nphot_avg, tmpdf.CALCS, label='data',linestyle='', marker='o')
ax.plot(tmpdf.Nphot_avg, calcs_fn([tmpdf.Nphot_avg, tmpdf.XeMultVoltag], popt[0], popt[1]),
         label='fit', linestyle='', marker='o')
ax.legend()

popt, pcov = scipy.optimize.curve_fit(calcs_fn,
                                      [ physdf.Nphot_avg, physdf.XeMultVoltag ],
                                      physdf.CALCS,
                                      [-40.0, 1.0],
                                      maxfev=10000
                                     )

popt

fig, ax = plt.subplots()
ax.plot(physdf.Nphot_avg, physdf.CALCS, label='data',linestyle='', marker='o')
ax.plot(physdf.Nphot_avg, calcs_fn([physdf.Nphot_avg, physdf.XeMultVoltag], popt[0], popt[1]),
         label='fit', linestyle='', marker='o')
ax.legend()


# measured vs. predicted
plt.plot(physdf.CALCS,
         calcs_fn([physdf.Nphot_avg, physdf.XeMultVoltag], popt[0], popt[1]),
         linestyle='', marker='o', alpha=0.6)

fig,ax = plt.subplots()
v_ax = np.linspace(physdf.XeMultVoltag.min(),physdf.XeMultVoltag.max(), 100)
# we get the gain by setting Nphot=1.0
ax.plot(v_ax, -calcs_fn([np.full(v_ax.shape, 1.0 ), v_ax], popt[0], popt[1]))
ax.set_yscale('log')
ax.set_xlabel('XeMultVoltag')
ax.set_ylabel('Gain')



